cimport numpy as np
import numpy as np
import time
from cython.parallel import prange
cimport cython
from cpython cimport array
import array
import sys
cimport openmp
from collections import OrderedDict


# @cython.boundscheck(False)
cdef int compareA(int a,int b,int [:]comp,bint fromSmall) nogil:
    # return (comp[a]-comp[b])*(fromSmall+(-1*(1-fromSmall)))
    cdef int res
    res=(comp[a]-comp[b])
    if res==0:
        res=a-b
    res*=(fromSmall+(-1*(1-fromSmall)))
    return res
# @cython.boundscheck(False)
cdef void merge(int[:] a,int lena,int [:] comp,int[:] newA,bint fromSmall) nogil:
    if lena<=1:
        return
    cdef int right=lena//2
    cdef int left=lena-right
    merge(a[:left],left,comp,newA[:left],fromSmall)
    merge(a[left:],right,comp,newA[left:],fromSmall)
   
#     print '!!! merged',np.asarray(left) ,np.asarray(right) ,np.asarray(a),np.asarray(newA)
    combinetwo(a[:left],left,a[left:],right,comp,newA,fromSmall)
    a[:left]=newA[:left]
    a[left:]=newA[left:]
# @cython.boundscheck(False)
cdef void combinetwo(int[:]left,int lenl,
                     int[:] right,int lenr,int [:]comp,int[:] newA,bint fromSmall) nogil:
    cdef int sl=0,sr=0,ni=0
    while sl<lenl and sr<lenr:
        
        if compareA(left[sl],right[sr],comp,fromSmall)<0:
            newA[ni]=left[sl]
            sl+=1
#             ni+=1
        else:
            newA[ni]=right[sr]
            sr+=1
        ni+=1
            
    while sl<lenl:
        newA[ni]=left[sl]
        sl+=1
        ni+=1
    while sr<lenr:
        newA[ni]=right[sr]
        sr+=1
        ni+=1
def mergesortC(a,comp,fromSmall=True):
    lena=len(a)
    newA=np.zeros((lena,),dtype=np.int32)
    # print a.dtype,comp.dtype,newA.dtype,type(lena)
    merge(a,lena,comp,newA,fromSmall)
    return newA
def sortPy(a,comp,fromSmall=True):
    return sorted(a,cmp=lambda x,y:comp[x]-comp[y],reverse=not fromSmall)
    # return a
#     print 'l r all',np.asarray(left) ,np.asarray(right) ,np.asarray(a),np.asarray(newA)     
def test_sorting_atonce():
    np.random.seed(1)
    nn=461000

    lenA,forV=nn,nn

    vertN=128
    comp=np.arange(1,vertN+1)
    np.random.shuffle(comp)

    ## HERE SET comp variant
    comp=np.array([5,1,2,3,7])
    vertN=len(comp)
    a=np.random.randint(0,vertN,(nn,))


    fromSmall=True
    
    stra=time.time()
    anPy=None
    anPy=sortPy(a,comp,fromSmall)
    timePy=time.time()-stra

    

    stra=time.time()
    anC=None
    anC=mergesortC(a,comp,fromSmall)
    timeC=time.time()-stra

    ordD=OrderedDict()
    for i in anPy:
        ordD[i]=i
    if len(ordD)<20: print 'dikt of additions,',ordD
    print 'dif_between_mergeC_and_sortPy(0):{}'.format(
        np.sum([a!=b for a,b in zip(anPy,anC)]),

    )
    print 'TimeC:{}; TimePy:{} '.format(timeC,timePy)
    print "\n=====================\n"
