from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
import numpy as np
import os

os.environ['PATH']=r'E:\programs\tdm_gcc4.7\bin;'+os.environ['PATH']
# print np.get_include()
ext_module = Extension(
    "sorting_parallel",
    ["sorting_parallel.pyx"],
    extra_compile_args=['-fopenmp'],
    extra_link_args=['-fopenmp'],
	include_dirs=[np.get_include()],
	language='c++'
)

setup(
   
    ext_modules = cythonize(ext_module),
)