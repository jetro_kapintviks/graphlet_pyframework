cimport numpy as np
import numpy as np
import time
from cython.parallel import prange,threadid

from cpython cimport array
import array
import sys

DEF CUTOFF = 17
DEF MAXDEGREE=10

cdef compareA(np.int_t a,np.int_t b,np.int_t*comp):
    return comp[a]-comp[b]



cdef sth( np.ndarray[np.int_t , ndim=1] a,int start,int end, np.ndarray[np.int_t , ndim=1] deg,int maxD):
    cdef np.ndarray[np.int_t , ndim=1]  newA=np.zeros((end-start,),dtype=np.int)
    
    
    cdef int i
    cdef int minDeg=18585682356,maxDeg=0
    for i in range(start,end):
        if deg[a[i]]>maxDeg:
            maxDeg=deg[a[i]]
    cdef np.ndarray[np.int_t , ndim=1]  degreeCnt=np.zeros((maxDeg+1,),dtype=np.int)
#     print 'maxDeg',maxDeg
    for i in range(start,end):
        degreeCnt[deg[a[i]] ]+=1
        if deg[a[i]]<minDeg:
            minDeg=deg[a[i]]
#     print 'degree Cnt',degreeCnt,minDeg
    
    cdef int work=0,tempDeg=0
    for i in range(minDeg,maxDeg):
        tempDeg=degreeCnt[i]
        degreeCnt[i]=work
        work+=tempDeg
       
#      print 'degree Cnt',degreeCnt,end-start
    for i in range(start,end):
        newA[degreeCnt[ deg[a[i]] ] ]=a[i]
        degreeCnt[ deg[a[i]] ] +=1
    return newA

cdef extern from "alloc_int.c":
    int* alloc_int(int n) nogil

cdef extern from "alloc_int.c":
    void free_int(int* n) nogil
cdef void sthP( int* a,int start,int end,int* deg,
         int[:] newA,) nogil:
    # with gil:
        # print 'here in sthP'
        # sys.stdout.flush()
    cdef int i
    cdef int minDeg=18585682356,maxDeg=0
    for i in range(start,end):
        if deg[a[i]]>maxDeg:
            maxDeg=deg[a[i]]
        if deg[a[i]]<minDeg:
            minDeg=deg[a[i]]
    maxDeg+=1
    cdef int* degreeCnt=alloc_int(maxDeg)
    for i in range(minDeg,maxDeg):
        degreeCnt[i]=0
#     print 'min/maxDeg',minDeg, maxDeg
    for i in range(start,end):
        degreeCnt[deg[a[i]] ]+=1
    
#     print 'degree Cnt',degreeCnt,minDeg
    
    cdef int work=0,tempDeg=0
    for i in range(minDeg,maxDeg):
        tempDeg=degreeCnt[i]
        degreeCnt[i]=work
        work+=tempDeg
        
    # with gil:
        # print 'starting {}:{}\n'.format(start,[sthd for sthd in degreeCnt])
        # sys.stdout.flush()
    
    cdef int tempThread=threadid()
#      print 'degree Cnt',degreeCnt,end-start
    for i in range(start,end):
        if (degreeCnt[ deg[a[i]] ] +start)>=121000:
            
            with gil:
                print 'exception: {} {} {}\n'.format(tempThread,start,end)
                sys.stdout.flush()
                
        newA[degreeCnt[ deg[a[i]] ]+start ]=a[i]
        degreeCnt[ deg[a[i]] ] +=1
    
    # with gil:
        # print 'second {}:{}\n'.format(start,[sthd for sthd in degreeCnt])
        # sys.stdout.flush()
    free_int(degreeCnt)   
cdef int testFuncP(int* a,int start,int end,int* deg,
         int[:] newA) nogil:
    return 6
#     return newA
        
cdef int[:] callS(int i,np.ndarray[np.int_t , ndim=1]  a,np.ndarray[np.int_t , ndim=1] comp,int [:] newA,
                    int[:]  degC):
    cdef int in_i,stop,step,to_in_i,sumi=0
    step=i//10
    stop=i-(i%step)
    
    
    
    cdef int* ca=<int*>a.data
    cdef int* ccomp=<int*>comp.data
    cdef int [:] aslice
#     cdef np.int_t* cnewA=<np.int_t*>newA.data
    
#     printA=np.asarray(<np.int_t[:len(newA)]> cnewA)
#     print printA[:100]
    for in_i in prange(0,stop,step,nogil=True):
        # with gil:
            # print 'in here before in prange:',in_i
            # sys.stdout.flush()
            
            
        # sumi=threadid()
        # to_in_i=in_i+step
        # with gil:
            # print 'in here in prange: {} {} {}\n'.format(sumi,in_i,to_in_i)
            # sys.stdout.flush()
        
        # aslice=newA[in_i:to_in_i]
        
        sthP(ca,in_i,in_i+step,ccomp,newA)
        # sumi+=testFuncP(ca,in_i,to_in_i,ccomp,newA)
       
#         print a[in_i:in_i+step],printA[in_i:in_i+step]
#         print 'stop step',in_i,in_i+step
    print 'testFunc',sumi
    sys.stdout.flush()
    return newA



# for ind,i in enumerate(range(1000,410100,3000)):
# # print a,np.sum(comp[a]==1)
#     stra=time.time()
#     ac=sth(a,0,i,comp,8)
#     sumC.append(time.time()-stra)
# # print 'c code',time.time()-stra

# for ind,i in enumerate(range(1000,410100,3000)):
# # print a,np.sum(comp[a]==1)
#     stra=time.time()
#     cdef np.ndarray[np.int_t , ndim=1]  newA=np.zeros((i,),dtype=np.int)
#     for in_i in prange(0,i-(i%(i//10)),i//10):
#         sthP(a,in_i,in_i+(i//10),comp,newA)
#     sumC.append(time.time()-stra)
# # print 'c code',time.time()-stra

# for ind,i in enumerate(range(1000,410100,3000)):
#     stra=time.time()
#     ap=sorted(a[:i],key=lambda x: comp[x])
#     sumP.append(time.time()-stra)

def test_speed():
    np.random.seed(1)
    nn=561000
    a=np.random.randint(0,5,(nn,))

    comp=np.array([5,1,2,3,7])
    sumC,sumP,sumCP=[],[],[]

    i=121000

    print a

    newA=np.zeros((i,),dtype=np.int)
    degC=np.zeros((MAXDEGREE,),dtype=np.int)


    ap=[]
    for in_i in range(0,i-(i%(i//10)),i//10):
        ap.extend(sorted(a[in_i:in_i+(i//10)],key=lambda x: comp[x]))
        print 'stop step',in_i,in_i+(i//10)

    pnewA=callS(i,a,comp,newA,degC)
    printA=pnewA
    # print 'a:{}\n, print a:{}\n, ap:{}\n'.format(a[:100],[pa for pa in printA[:100]],ap[:100])
    print 'py_code_vs_cParalel(0):{}'.format(np.sum([p!=cp for p,cp in zip(ap,printA)]),
                                             # np.where(np.array([p!=cp for p,cp in zip(ap,printA)])==1  )
                                             )

cdef int test_prange(Py_ssize_t n):
    cdef Py_ssize_t i

    for i in prange(n, nogil=True):
        i+=1
    return i
def Ptest_prange():
    return test_prange(2)