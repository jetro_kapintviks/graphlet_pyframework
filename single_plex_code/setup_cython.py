from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
import numpy as np
import os,sys

# print (sys.platform)


if 'linux' in sys.platform:
        pass
else:
	if 'GCC_4' not in os.environ:
		sys.exit("GCC_4 environment variable should point to location of gcc 4 folder")
	os.environ['PATH']=os.environ['GCC_4']+';'+os.environ['PATH']


# print np.get_include()
ext_module = Extension(
    "cython_funcs",
    ["cython_funcs.pyx"],
    extra_compile_args=['-fopenmp'],
    extra_link_args=['-fopenmp'],
	include_dirs=[np.get_include()],
	language='c++'
)

setup(
   
    ext_modules = cythonize(ext_module),
)
