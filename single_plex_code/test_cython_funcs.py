import os,sys

if 'GCC_4' not in os.environ:
	sys.exit("GCC_4 environment variable should point to location of gcc 4 folder")
os.environ['PATH']=os.environ['GCC_4']+';'+os.environ['PATH']

from cython_funcs import test_sorting_degrees,test_sorting_atonce,test_graph_decomposition

test_sorting_degrees()
test_sorting_atonce()
test_graph_decomposition()
test_graph_decomposition('test_struct2.csv','testing.csv')
