import numpy as np
from enum import Enum 
from functools import partial
import os 

if 'GCC_4' not in os.environ:
	sys.exit("GCC_4 environment variable should point to location of gcc 4 folder")
os.environ['PATH']=os.environ['GCC_4']+';'+os.environ['PATH']

from cython_funcs import sortAllC,sortAllPy,mergesortC,sortPy,get_orbitals
import sys,time
class SortDegree(Enum):
    C=1
    CParallel=2
    Py=3
    PyBucket=4

class SortEdge(Enum):
    MergeC=1
#     CParallel=2
    Py=3
#     PyBucket=4
class Graph_Extended:
    sort_degree={
        SortDegree.C: partial(sortAllC,doParallel=False),
        SortDegree.CParallel: partial(sortAllC,doParallel=True),
        SortDegree.Py: partial(sortAllPy,doBucket=False),
        SortDegree.PyBucket :partial(sortAllPy,doBucket=True),
    }
    sort_edges={
        SortEdge.MergeC:mergesortC,
#         SortEdge.BucketC:,
        SortEdge.Py:sortPy,
    }
    
    def __init__(self,sortAlg=SortDegree.C,sortEAlg=SortEdge.MergeC,efromSmall=False):
        self.sortAlg=sortAlg
        self.sortEAlg=sortEAlg
        self.EfromSmall=efromSmall
        pass
    def load_file(self,inputfile,delimiter=',',**kwargs):
        self.inputfile=inputfile
        
        graph_table=np.loadtxt(inputfile,delimiter=delimiter,dtype=int,**kwargs)
        fix_graph_table=np.zeros((len(graph_table),2),dtype=int)
        curVInd=0
        vertDikt={}
        for ind,(i,j) in enumerate(graph_table):
            if i not in vertDikt:
                vertDikt[i]=curVInd
                curVInd+=1
            if j not in vertDikt:
                vertDikt[j]=curVInd
                curVInd+=1

            fix_graph_table[ind]=(vertDikt[i],vertDikt[j])
#             if ind%12000==2: print("\r iterat:{}".format(ind),end='',flush=True)
        self.vertN=curVInd
        self.graph_t=fix_graph_table
        
        return fix_graph_table,curVInd

    def create_edges_vertices(self):
        vertices=np.zeros((self.vertN+1,),dtype=int)
        for i,j in self.graph_t:
            vertices[i]+=1
            vertices[j]+=1
        edgesL=[np.zeros((i,),dtype=int) for i in vertices]
        edgesC=np.zeros((self.vertN,),dtype=int)
        for i,j in self.graph_t:
            edgesL[i][edgesC[i]]=j
            edgesL[j][edgesC[j]]=i

            edgesC[i]+=1
            edgesC[j]+=1

        startingV=vertices[0]
        vertices[0]=0
        for i in range(1,len(vertices)):
            si=vertices[i]
            vertices[i]=startingV
            startingV+=si
            
    
        self.edgesAll=np.concatenate([i for i in edgesL])
        self.verticesInd=vertices
        return self.edgesAll,vertices
    def count_degrees(self):
        self.degrees=np.zeros((self.vertN,),dtype=int)
        for ind,(sec,fir) in enumerate(zip(self.verticesInd[1:],self.verticesInd)):
            self.degrees[ind]=sec-fir
        
        self.maxD=np.max(self.degrees)
        return self.degrees
    def sort_degrees(self):
        self.edgesAll=Graph_Extended.sort_degree[self.sortAlg] \
        (self.edgesAll,self.degrees,self.verticesInd)
        return self.edgesAll
    
    def create_vu_maps(self):
        uniqEdges=len(self.edgesAll)//2
        self.v_map=np.zeros((uniqEdges,),dtype=int)
        self.u_map=np.zeros((uniqEdges,),dtype=int)
        mapI=0
        for indV,(svertI,evertI) in enumerate(zip(self.verticesInd,self.verticesInd[1:])):
            for sindV,uvertI in enumerate(self.edgesAll[svertI:evertI]):
                if indV>=uvertI:
                    continue
                if self.degrees[indV]<self.degrees[uvertI]:
                    self.v_map[mapI]=uvertI
                    self.u_map[mapI]=indV
                else:
                    self.v_map[mapI]=indV
                    self.u_map[mapI]=uvertI
                
                mapI+=1
                
        return self.v_map,self.u_map
    def create_edge_map(self):
        edgeInd=np.zeros((len(self.edgesAll),) ,dtype=int)
        edgePos=np.zeros((len(self.edgesAll),) ,dtype=np.int8)
        
        vertC=self.verticesInd.copy()
        for ind,(v,u) in enumerate(zip(self.v_map,self.u_map)):
            edgeInd[vertC[v]]=ind
            edgeInd[vertC[u]]=ind
            
            edgePos[vertC[v]]=True
            edgePos[vertC[u]]=False
            
            vertC[v]+=1
            vertC[u]+=1
        
        self.edgeInd=edgeInd
        self.edgePos=edgePos
        return edgeInd,edgePos
    def sort_edges_for_PGD(self):
        leneMap=len(self.v_map)
        comp=np.zeros((leneMap,),dtype=int)
        sortInd=np.arange(leneMap)
        for ind,(v_e,u_e) in enumerate(zip(self.v_map,self.u_map)):
            comp[ind]=self.degrees[v_e]+self.degrees[u_e]
        print comp[:20],self.v_map[:20],self.u_map[:20]
        sortInd=Graph_Extended.sort_edges[self.sortEAlg](sortInd,comp,fromSmall=self.EfromSmall)
        
        self.v_map=self.v_map[sortInd]
        self.u_map=self.u_map[sortInd]
        
        return self.v_map,self.u_map
    def graphlet_decomposition(self):
        orbitalC,EdgeC=get_orbitals(self)
        self.orbitalC=orbitalC
        return orbitalC
    def save_orbitals(self,inputfile=None,delimiter=',',
                   header=None,fmt='%6d',comments='',**kwargs):
        if not inputfile:
            inputfile=self.inputfile.split('.')[0]+'_orbitals.csv'
        if not header:
            header='counts for first 15 graphlet orbitals [vertices,orbital]'
            header+='\n'+'{0:>6}'.format('vert_i')+delimiter+ \
            delimiter.join(['{0:>6}'.format('orb_'+str(i)) for i in range(15)])
        np.savetxt(inputfile,np.hstack((np.arange(self.vertN).reshape(-1,1),self.orbitalC)),delimiter=delimiter,
                   header=header,fmt=fmt,comments='',**kwargs)        
        
def test_graph(fileN):
    graphE=Graph_Extended()
    graph_t,vertN=graphE.load_file(fileN)
    edges,vertices=graphE.create_edges_vertices()
    degrees=graphE.count_degrees()
    sedges=graphE.sort_degrees()
    vm,um=graphE.create_vu_maps()
    vm,um=graphE.sort_edges_for_PGD()
    ei,ep=graphE.create_edge_map()
    print("vertN:{}\nedges:{},\nvertices:{}\ndegrees:{}\n"
    "sortEdges{}\nv_map,u_map{},{}\nedge map {}\nedgeP {}\n".format(
            vertN,edges,vertices[:20]
            ,degrees[:20],sedges,vm[:20],um[:20],ei[:20],ep[:20]))
            
    return graphE


def test_graph_decomp(fileN,**kwargs):
    graphE = Graph_Extended()
    graph_t, vertN = graphE.load_file(fileN,**kwargs)
    edges, vertices = graphE.create_edges_vertices()
    degrees = graphE.count_degrees()
    sedges = graphE.sort_degrees()
    vm, um = graphE.create_vu_maps()
    vm, um = graphE.sort_edges_for_PGD()
    ei, ep = graphE.create_edge_map()

    print 'starting graphlet \n\n'
    sys.stdout.flush()
    count = time.time()
    oc = graphE.graphlet_decomposition()
    count = time.time() - count

    graphE.save_orbitals()

    print 'time for this graph: ', count
    print("vertN:{}\nedges:{},\nvertices:{}\ndegrees:{}\n"
          "sortEdges{}\nv_map,u_map{},{}\nedge map {}\nedgeP {}\n".format(
        vertN, edges, vertices[:20]
        , degrees[:20], sedges, vm[:20], um[:20], ei[:20], ep[:20]))

    return graphE

if __name__ == "__main__":
    import yappi,cProfile,pstats
    
    cProfile.runctx("test_graph_decomp(r'PGD-master\data\C500-9.mtx',delimiter=' ',skiprows=2)", globals(), locals(), "Profile.prof")
    s = pstats.Stats("Profile.prof")
    s.strip_dirs().sort_stats("time").print_stats()
    # cProfile.run("test_graph_decomp(r'PGD-master\data\C500-9.mtx',delimiter=' ',skiprows=2)")
