# cython: profile=True


include "sorting_degrees.pyx"

include "sorting_atonce.pyx"

include "graph_decomposition.pyx"
