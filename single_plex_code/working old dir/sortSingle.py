import numpy as np
def sortSingle(baseL,newL,deg,start):
#     start,stop=startstop
    
    minD,maxD=8753856835,0
    for i in baseL:
        if minD>deg[i]:
            minD=deg[i]
        if maxD<deg[i]:
            maxD=deg[i]
    maxD+=1
    degCount=np.zeros((maxD,),dtype=int)
    for i in baseL:
        degCount[deg[i]]+=1
    work=0
    for i,x in enumerate(degCount):
        degCount[i],work=work,work+degCount[i]
    
    for i in baseL:
        newL[degCount[deg[i] ] +start]=i
        degCount[deg[i]]+=1
    # print newL
def sortSingleMap(baseLnewL):
#     start,stop=startstop
    baseL,newL=baseLnewL
    deg=np.array([5,1,2,3,7])
    minD,maxD=8753856835,0
    for i in baseL:
        if minD>deg[i]:
            minD=deg[i]
        if maxD<deg[i]:
            maxD=deg[i]
    maxD+=1
    degCount=np.zeros((maxD,),dtype=int)
    for i in baseL:
        degCount[deg[i]]+=1
    work=0
    for i,x in enumerate(degCount):
        degCount[i],work=work,work+degCount[i]
    
    for i in baseL:
        newL[degCount[deg[i] ] ]=i
        degCount[deg[i]]+=1
    # print newL
    return newL