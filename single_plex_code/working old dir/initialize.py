
def load_file(inputfile,delim):
	graph_table=np.loadtxt(inputfile,delim=delim)
	fix_graph_table=graph_table
	curVInd=0
	for i,j in graph_table:
		if i not in vertDikt:
			vertDikt[i]=curVInd
			curVInd+=1
		if j not in vertDikt:
			vertDikt[j]=curVInd
			curVInd+=1
		
		fix_graph_table=np.vstack((fix_graph_table,(vertDikt[i],vertDikt[j]) )
		