
# coding: utf-8

# In[2]:

get_ipython().magic(u'load_ext cython')
get_ipython().magic(u'load_ext line_profiler')
import line_profiler


# In[26]:

get_ipython().run_cell_magic(u'cython', u' --compile-args=-DCYTHON_TRACE=1', u"cimport numpy as np\nimport numpy as np\nimport time\nfrom cython.parallel import prange\n\nfrom cpython cimport array\nimport array\nDEF CUTOFF = 17\nDEF MAXDEGREE=10\n\ncdef compareA(np.int_t a,np.int_t b,np.int_t*comp):\n    return comp[a]-comp[b]\n\n\n\ncdef sth( np.ndarray[np.int_t , ndim=1] a,int start,int end, np.ndarray[np.int_t , ndim=1] deg,int maxD):\n    cdef np.ndarray[np.int_t , ndim=1]  newA=np.zeros((end-start,),dtype=np.int)\n    \n    \n    cdef int i\n    cdef int minDeg=18585682356,maxDeg=0\n    for i in range(start,end):\n        if deg[a[i]]>maxDeg:\n            maxDeg=deg[a[i]]\n    cdef np.ndarray[np.int_t , ndim=1]  degreeCnt=np.zeros((maxDeg+1,),dtype=np.int)\n#     print 'maxDeg',maxDeg\n    for i in range(start,end):\n        degreeCnt[deg[a[i]] ]+=1\n        if deg[a[i]]<minDeg:\n            minDeg=deg[a[i]]\n#     print 'degree Cnt',degreeCnt,minDeg\n    \n    cdef int work=0,tempDeg=0\n    for i in range(minDeg,maxDeg):\n        tempDeg=degreeCnt[i]\n        degreeCnt[i]=work\n        work+=tempDeg\n       \n#      print 'degree Cnt',degreeCnt,end-start\n    for i in range(start,end):\n        newA[degreeCnt[ deg[a[i]] ] ]=a[i]\n        degreeCnt[ deg[a[i]] ] +=1\n    return newA\n\n\ncdef void sthP( np.int_t* a,int start,int end,np.int_t* deg,\n         int[:] newA,int[:] degreeCnt) nogil:\n    cdef int i\n    cdef int minDeg=18585682356,maxDeg=0\n    for i in range(start,end):\n        if deg[a[i]]>maxDeg:\n            maxDeg=deg[a[i]]\n        if deg[a[i]]<minDeg:\n            minDeg=deg[a[i]]\n    maxDeg+=1\n    for i in range(minDeg,maxDeg):\n        degreeCnt[i]=0\n#     print 'min/maxDeg',minDeg, maxDeg\n    for i in range(start,end):\n        degreeCnt[deg[a[i]] ]+=1\n    \n#     print 'degree Cnt',degreeCnt,minDeg\n    \n    cdef int work=0,tempDeg=0\n    for i in range(minDeg,maxDeg):\n        tempDeg=degreeCnt[i]\n        degreeCnt[i]=work\n        work+=tempDeg\n       \n#      print 'degree Cnt',degreeCnt,end-start\n    for i in range(start,end):\n        newA[degreeCnt[ deg[a[i]] ] ]=a[i]\n        degreeCnt[ deg[a[i]] ] +=1\n    \n#     return newA\n        \nnp.random.seed(1)\nnn=561000\na=np.random.randint(0,5,(nn,))\n\ncomp=np.array([5,1,2,3,7])\nsumC,sumP,sumCP=[],[],[]\n\n\n\n# for ind,i in enumerate(range(1000,410100,3000)):\n# # print a,np.sum(comp[a]==1)\n#     stra=time.time()\n#     ac=sth(a,0,i,comp,8)\n#     sumC.append(time.time()-stra)\n# # print 'c code',time.time()-stra\n\n# for ind,i in enumerate(range(1000,410100,3000)):\n# # print a,np.sum(comp[a]==1)\n#     stra=time.time()\n#     cdef np.ndarray[np.int_t , ndim=1]  newA=np.zeros((i,),dtype=np.int)\n#     for in_i in prange(0,i-(i%(i//10)),i//10):\n#         sthP(a,in_i,in_i+(i//10),comp,newA)\n#     sumC.append(time.time()-stra)\n# # print 'c code',time.time()-stra\n\n# for ind,i in enumerate(range(1000,410100,3000)):\n#     stra=time.time()\n#     ap=sorted(a[:i],key=lambda x: comp[x])\n#     sumP.append(time.time()-stra)\n\n\ni=121000\n\nprint a\n\nnewA=np.zeros((i,),dtype=np.int)\ndegC=np.zeros((MAXDEGREE,),dtype=np.int)\ncdef np.int_t* callS(np.ndarray[np.int_t , ndim=1]  a,np.ndarray[np.int_t , ndim=1] comp,np.ndarray[np.int_t , ndim=1] newA,\n                    np.ndarray[np.int_t , ndim=1]  degC):\n    cdef int in_i,stop,step,to_in_i\n    stop=i-(i%(i//10))\n    step=i//10\n    \n    \n    cdef np.int_t* ca=<np.int_t*>a.data\n    cdef np.int_t* ccomp=<np.int_t*>comp.data\n    cdef np.int_t* cnewA=<np.int_t*>newA.data\n    \n    printA=np.asarray(<np.int_t[:len(newA)]> cnewA)\n#     print printA[:100]\n    for in_i in range(0,stop,step):\n        sthP(ca,in_i,in_i+step,ccomp,newA[in_i:in_i+step],degC)\n#         print a[in_i:in_i+step],printA[in_i:in_i+step]\n        print 'stop step',in_i,in_i+step\n    return cnewA\n\nap=[]\nfor in_i in range(0,i-(i%(i//10)),i//10):\n    ap.extend(sorted(a[in_i:in_i+(i//10)],key=lambda x: comp[x]))\n    print 'stop step',in_i,in_i+(i//10)\n\ncdef np.int_t* pnewA=callS(a,comp,newA,degC)\nprintA=np.asarray(<np.int_t[:len(newA)]> pnewA)\n# print 'a, print a , ap:',a,printA,ap\nprint 'py_code_vs_cParalel(0):{}'.format(np.sum([p!=cp for p,cp in zip(ap,printA)]),\n                                         np.where(np.array([p!=cp for p,cp in zip(ap,printA)])==1  ))")


# In[3]:

get_ipython().magic(u'matplotlib inline')
import matplotlib.pyplot as plt

plt.figure(figsize=(20,20))

plt.plot(np.arange(1000,410100,3000),sumC,c='y')
plt.plot(np.arange(1000,410100,3000),sumP,c='b')
# plt.xlim([1000,12000])


# In[ ]:

# def csort(np.ndarray[np.int_t,ndim=1] a,np.ndarray[np.int_t,ndim=1] compar):
# #     qsort(<np.int_t*>a.data,compar, 0, a.shape[0])
#     insertion_sort(<np.int_t*>a.data,<np.int_t*>compar.data, 0, a.shape[0])
# cdef void qsort(np.int_t* a,np.ndarray[np.int_t,ndim=1] comp, Py_ssize_t start, Py_ssize_t end):
# #     print start ,end
#     if (end - start) <=1:
# #         insertion_sort(a,comp, start, end)
#         return
#     cdef Py_ssize_t boundary = partition(a,comp, start, end)
#     cdef int i
# #     print 'boundry',boundary,[a[i] for i in range(start,end)]
# #     return
#     qsort(a,comp, start, boundary)
#     qsort(a,comp, boundary+1, end)

# cdef Py_ssize_t partition(np.int_t* a,np.ndarray[np.int_t,ndim=1] comp, Py_ssize_t start, Py_ssize_t end):
#     assert end > start
#     cdef Py_ssize_t i = start, j = end-1
#     cdef np.int_t pivot = a[j]
#     while True:
#         # assert all(x < pivot for x in a[start:i])
#         # assert all(x >= pivot for x in a[j:end])

#         while compareA(a[i],pivot,comp)<0:
#             i += 1
#         while i < j and compareA(a[j],pivot,comp)>=0:
#             j -= 1
#         if i >= j:
#             break
       
#         swap(a, i, j)
        
    
#     swap(a, i, end-1)
#     assert a[i] == pivot
#     # assert all(x < pivot for x in a[start:i])
#     # assert all(x >= pivot for x in a[i:end])
#     return i

# cdef inline void swap(np.int_t* a, Py_ssize_t i, Py_ssize_t j):
# #     print 'swap', a[i],a[j]
#     a[i], a[j] = a[j], a[i]
# #     print 'swap', a[i],a[j]
            
    
    
# cdef void insertion_sort(np.int_t* a,np.int_t*comp, Py_ssize_t start, Py_ssize_t end):
#     cdef Py_ssize_t i, j
#     cdef np.int_t v
#     for i in range(start, end):
#         #invariant: [start:i) is sorted
#         v = a[i]; j = i-1
#         while j >= start:
#             if compareA(a[j],v,comp)<=0: break
#             a[j+1] = a[j]
#             j -= 1
#         a[j+1] = v

