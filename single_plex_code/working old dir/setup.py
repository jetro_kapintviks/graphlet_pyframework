from distutils.core import setup
from Cython.Build import cythonize
from Cython.Distutils import build_ext
from setuptools import Extension
import numpy

setup(name='Graphlet Cython Functions',
      ext_modules=[Extension('graph_ext', ['graph_ext.pyx'],
                             include_dirs=[numpy.get_include()])],
      cmdclass = {'build_ext':build_ext},
      extra_compile_args=['-fopenmp'],
    extra_link_args=['-fopenmp'],
      )