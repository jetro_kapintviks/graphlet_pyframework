
from libc.stdlib cimport abort,malloc,free,exit
cimport numpy as np
import numpy as np
import time
from cython.parallel import prange
cimport cython
from cpython cimport array
import array
import sys
cimport openmp
from cython.operator cimport dereference as deref,preincrement as inc
from cython.parallel import parallel, prange




@cython.boundscheck(False)
cdef void mark_neighbors(int v,int u,int[:] vertexes,
						 int[:]edges,int* ind,int* T_count,int maxD,int& oneC) nogil:
	cdef int vert,w
	for vert in range(vertexes[v],vertexes[v+1]):
		w=edges[vert]
		if w==u:
			continue
		ind[w]=1
		# T_count[maxD-oneC]=w
		# inc(oneC)

@cython.boundscheck(False)
cdef void clear_ind(int v,int u,int[:] vertexes,int[:]edges,int* ind) nogil:
	cdef int vert
	for vert in range(vertexes[v],vertexes[v+1]):
		ind[edges[vert]]=0

@cython.boundscheck(False)
cdef void triangles_wedges(int v,int u,
					  int[:] vertexes,int[:] edges,
					  int* ind,int* T_count,int* W_count,
					  int& tri_n,int& w_n,int maxD, int & oneC,int lenV) nogil:
#	 with gil:
#		 if v==58: print "starting, u: {} v: {}\nind:{}\n".format(u,v,np.asarray(<int[:lenV]>ind))

	cdef int vI,w
	for vI in range(vertexes[u],vertexes[u+1]):
		w=edges[vI]
		if w==v:
			continue
		if ind[w]==1:
			ind[w]=3
			T_count[tri_n]=w
			inc(tri_n)
		else:
			W_count[w_n]=w
			inc(w_n)
			ind[w]=2

	for vI in range(vertexes[v],vertexes[v+1]):
		w=edges[vI]
		if ind[w]==1:
			T_count[maxD-oneC]=w
			inc(oneC)
#	 with gil:
#		 if v==58: print "ending, u: {} v: {}\nind:{}\n".format(u,v,np.asarray(<int[:lenV]>ind))

@cython.boundscheck(False)
cdef void init_array(int* ar,int lena,int val) nogil:
	for i in range(lena):
		ar[i]=0

@cython.boundscheck(False)
cdef void cycles(int* W_count,int w_n,int& cycle_n,int & bfree_n,
				 int& btri_n, int& bcliq_n,int[:] vertexes,
				int[:] edges,int[:] degrees,int* ind,int v,int u) nogil:
	cdef int inW,in_neighW,w,wNeigh
	for inW in range(0,w_n):
		w=W_count[inW]
		for in_neighW in range(vertexes[w],vertexes[w+1]):
			wNeigh=edges[in_neighW]
			if ind[wNeigh]==1:
				inc(cycle_n)
			elif ind[wNeigh]==0 and wNeigh!=u:
				inc(bfree_n)
			if ind[wNeigh]==2:
				inc(btri_n)
			elif ind[wNeigh]==3:
				inc(bcliq_n)

#		 W_count[inW]=0
			ind[w]=0
#			 if v==1 and u==0:
#				 with gil:
#					 print "wneugh {} indW {}\n".format(wNeigh,ind[wNeigh])
@cython.boundscheck(False)
cdef void cliques(int* T_count,int tri_n,int& clique_n,
				  int & tcliq_n,int& tfree_n,int& ttri_n,
				  int maxD,int oneC,int[:] vertexes,
				int[:] edges,int [:] degrees, int* ind,int v,int u) nogil:
	cdef int inT,in_neighT,w,indNeigh,inO,w2,wNeigh
	for inT in range(0,tri_n):
		w=T_count[inT]
		for in_neighT in range(vertexes[w],vertexes[w+1]):
			indNeigh=ind[edges[in_neighT]]
			if indNeigh==3:
				inc(clique_n)
			elif indNeigh==1:
				inc(tcliq_n)
		ind[w]=4
	for inO in range(maxD,maxD-oneC,-1):
		w=T_count[inO]
		for w2 in range(vertexes[w],vertexes[w+1]):
			wNeigh=edges[w2]

			if ind[w]==1 and ind[wNeigh]==0 and wNeigh!=v:
				inc(tfree_n)
			elif ind[w]==1 and ind[wNeigh]==1:

				inc(ttri_n)

#			 if v==0 and u==4 and wNeigh!=v:
#				 with gil:
#					 print 'w:{} wneigh:{}; ind[w]:{}; ind[Wneigh]:{} \n'.format(\
#						 w,wNeigh,ind[w],ind[wNeigh])

		ind[w]=4
#		 T_count[inT]=0



@cython.boundscheck(False)
cdef np.ndarray[np.int32_t,ndim=2] run_graph_decomp(int[:] e_v,int[:] e_u,
	int[:]degrees,int[:]vertexes,int[:]edges, int lenVU,int maxD,int lenV):

	cdef int[:,:] counts=np.zeros((12,lenVU),dtype=np.int32)
	cdef int* T_count=NULL
	cdef int* W_count=NULL
	cdef int v,u,degv,degu,tri_n,w_n,cycle_n,clique_n,oneC
	cdef int e_i,inI
	with nogil,parallel():

		T_count = <int *> malloc(sizeof(int) * (maxD+1))
		W_count = <int *> malloc(sizeof(int) * (maxD+1))
		ind= <int *> malloc(sizeof(int) * (lenV))
		if T_count==NULL or W_count==NULL or ind==NULL:
			exit(-3)
		init_array(T_count,maxD+1,0)
		init_array(W_count,maxD+1,0)
		init_array(ind,lenV,0)

		for e_i in prange(lenVU):
			v=e_v[e_i]
			u=e_u[e_i]
			degv=degrees[v]
			degu=degrees[u]
			tri_n=0
			w_n=0
			cycle_n=0
			clique_n=0
			oneC=0

			mark_neighbors(v,u,vertexes,edges,ind,T_count,maxD,oneC)
			triangles_wedges(v,u,vertexes,edges,ind,
							 T_count,W_count,tri_n,w_n,maxD,oneC,lenV)



			cycles(W_count,w_n,cycle_n,
				   counts[4,e_i],counts[5,e_i],counts[6,e_i],
					vertexes,edges,degrees,ind,v,u)
			cliques(T_count,tri_n,clique_n,
					counts[7,e_i],counts[8,e_i],counts[9,e_i],maxD,oneC,
					vertexes,edges,degrees,ind,v,u)

			counts[0,e_i]=tri_n
			counts[1,e_i]=w_n
			counts[2,e_i]=cycle_n
			counts[3,e_i]=clique_n
			counts[10,e_i]=oneC


			clear_ind(v,u,vertexes,edges,ind)
#			 with gil:
#				 print 'tcount2{}'.format(np.asarray(<int[:maxD+1]>T_count))

			with gil:
				pass
#				 print 'v: {} u:{} \nbfree:{} tfree:{}; ttri: {}; btri:{} ; cycle: {} \n'.format(\
#						 v,u,counts[4,e_i],counts[8,e_i],counts[9,e_i],counts[5,e_i],
#						 counts[2,e_i])
#				 print 'tcount{},wcount{}'.format(
#					 np.asarray(<int[:maxD+1]>T_count), \
#				 np.asarray(<int[:maxD+1]>W_count))
#				 sys.stdout.flush()
#		 print np.asarray(<int[:maxD+1]>T_count)
		free(T_count)
		free(W_count)
		free(ind)
	return np.asarray(counts)

@cython.cdivision(True)
@cython.boundscheck(False)
cdef np.ndarray[np.int32_t,ndim=2] counts_forV(
	int [:,:] counts, int [:] edge_map,
	np.int8_t[:] edge_pos,
	int[:]vertices,int [:] v_map,int[:] u_map, int [:] degrees,int lenV):

	cdef int in_i,ed_i,v_i,twos_v,pos,twos_u,deg,vv,uu,
	cdef int[:,:] v_counts=np.zeros((15,lenV),dtype=np.int32)

	for in_i in prange(lenV,nogil=True):

		for v_i in range(vertices[in_i],vertices[in_i+1]):
			ed_i=edge_map[v_i]
			pos=edge_pos[v_i]

			vv=v_map[ed_i]
			uu=u_map[ed_i]

			twos_v=degrees[vv]-counts[0,ed_i]-1
			twos_u=degrees[uu]-counts[0,ed_i]-1

			if  pos:
				v_counts[2,in_i]+=twos_v
				v_counts[1,in_i]+=twos_u
				v_counts[7,in_i]+=(twos_v*
					(twos_v-1)/2)-counts[9,ed_i]
				v_counts[9,in_i]+=counts[5,ed_i]
				v_counts[11,in_i]+=counts[9,ed_i]
				v_counts[6,in_i]+=(twos_u*
					(twos_u-1)/2)-counts[5,ed_i]
				v_counts[12,in_i]+=counts[6,ed_i]
				v_counts[10,in_i]+=(counts[0,ed_i]*(twos_u))-counts[6,ed_i]
				v_counts[4,in_i]+=(counts[4,ed_i])-counts[5,ed_i]

			else:
				v_counts[2,in_i]+=twos_u
				v_counts[1,in_i]+=twos_v
				v_counts[7,in_i]+=(twos_u*
					(twos_u-1)/2)-counts[5,ed_i]
				v_counts[9,in_i]+=counts[9,ed_i]
				v_counts[11,in_i]+=counts[5,ed_i]
				v_counts[6,in_i]+=(twos_v*
					(twos_v-1)/2)-counts[9,ed_i]
				v_counts[12,in_i]+=counts[7,ed_i]
				v_counts[10,in_i]+=(counts[0,ed_i]*(twos_v))-counts[7,ed_i]
				v_counts[4,in_i]+=counts[8,ed_i]-counts[2,ed_i]

			v_counts[5,in_i]+=(twos_u*twos_v)-counts[2,ed_i]
			v_counts[3,in_i]+=counts[0,ed_i]
			v_counts[8,in_i]+=counts[2,ed_i]
			v_counts[14,in_i]+=counts[3,ed_i]
			v_counts[13,in_i]+=(counts[0,ed_i]*
								(counts[0,ed_i]-1)/2)-counts[3,ed_i]

#			with gil:
#				print 'v {} u: {}, in_i {}; tri: {} t8:{} \n' \
#				.format(vv,uu,in_i,counts[0,ed_i],counts[8,ed_i])   

		v_counts[3,in_i]/=2
		v_counts[0,in_i]=degrees[in_i]
		v_counts[2,in_i]/=2
		v_counts[8,in_i]/=2
		v_counts[14,in_i]/=3
		v_counts[7,in_i]/=3
		v_counts[12,in_i]/=2

	return np.asarray(v_counts)
def comp2(x,y):
	res=x[0]-y[0]
	if res==0:
		return x[1]-y[1]
	return res

def get_orbitals(graphE):

	counts=None
	start=None
	counts=run_graph_decomp(graphE.v_map,graphE.u_map,
				 graphE.degrees,graphE.verticesInd,
				 graphE.edgesAll,len(graphE.v_map),graphE.maxD,graphE.vertN)

	start=counts_forV(counts,graphE.edgeInd,graphE.edgePos,graphE.verticesInd,
				  graphE.v_map,graphE.u_map,graphE.degrees,graphE.vertN)
	return start.T,counts



def test_graph_decomposition(graphName=None,vertGraphName=None):
	from graphExtended import test_graph

	if graphName==None or vertGraphName==None:
		graphName = 'PGD-master/data/small_2600.csv'
		vertGraphName="PGD-master/data/small_2600_vertmicro.csv"
	# PGD-master/data/small_2600.csv

	edgeGraphName,eargs="PGD-master/data/small_2600_micro.csv",{
	'skiprows':1,'delimiter':',','dtype':int
	}
	vargs={
	'skiprows':1,'delimiter':'\t','usecols':(0,2,3,4)
	}

	graphE=test_graph(graphName)


	stra=time.time()

	start,counts=get_orbitals(graphE)
	enda=time.time()-stra
	print 'time for this graph {}: '.format(graphName),enda

	triMat=np.vstack((graphE.v_map+1,graphE.u_map+1,counts)).T




	# headerC='start\tend\ttric\tstar2\tcycles\tcliques'


	# np.savetxt('all_counts.csv',triMat,delimiter='\t',
			   # fmt='%d',header=headerC,comments='')

	# np.savetxt("vert_counts.csv",
			# np.vstack((np.arange(graphE.vertN),start)),
			   # delimiter=',',fmt='%d',comments=''  )




	# realMat=np.loadtxt(edgeGraphName,
	# 				   **eargs)
    #
    #
	# realMat=sorted(realMat,cmp=comp2)
	# triMat=sorted(triMat,cmp=comp2)
	# triMat=np.array(triMat)
	# realMat=np.array(realMat)
    #
	# print 'compare triang(0): {}; cycles(0): {}; cliques(0): {}\n'.format(
	# np.sum([a!=b for a,b in zip(realMat[:,2],triMat[:,2])]),
	# 	np.sum([a!=b for a,b in zip(realMat[:,7],triMat[:,4])]),
	# 	np.sum([a!=b for a,b in zip(realMat[:,4],triMat[:,5])]),
	# )



	vertMat=np.loadtxt(vertGraphName,
					   **vargs)

	wvertMat=vertMat[vertMat[:,1]<=14,2].reshape((-1,15)).astype(int)

	# print wvertMat,start.T

	print 'compare 15 first orbitals of verts[row: 0/{}] (0):{} \n'.format(len(wvertMat),
	np.sum(np.sum(start[:len(wvertMat)]!=wvertMat)),

	)

	print '\n=================\n'

def test_Cytoscope_orbitals_M(vertGraphName,orbitalC):

	vargs={
	'skiprows':1,'delimiter':'\t','usecols':(0,2,3,4)
	}

	vertMat=np.loadtxt(vertGraphName,
		   **vargs)

	wvertMat=vertMat[vertMat[:,1]<=14,2].reshape((-1,15)).astype(int)

	# print wvertMat,start.T
	# print 'orbital wvert',orbitalC,wvertMat
	# np.savetxt("wvert.csv",wvertMat,fmt="%d")
	# np.savetxt("orbital.csv",orbitalC,fmt="%d")

	print 'compare 15 first orbitals of verts[row: 0/{}] (0):{} \n'.format(len(wvertMat),
	np.sum(np.sum(orbitalC[:len(wvertMat)]!=wvertMat)),

	)

	print '\n=================\n'
