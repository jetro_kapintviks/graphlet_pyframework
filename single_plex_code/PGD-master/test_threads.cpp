// Example program
#include <iostream>
#include <string>
#include <omp.h>
#include <vector>
#include <sstream>

using namespace std;
int main()
{
  
  vector<int>  v(121000,1);
  // for (int i=0;i<12;i++){
	  // cout<<"vec: "<<v[i]<<endl;
  // }
  int j=0;
  omp_set_num_threads(16);
  int n=16;
  #pragma omp parallel for schedule(dynamic) \
			 firstprivate(j,v) lastprivate(j,v)
		for (int i=0;i<n;i++){
			
			ostringstream sout;
		sout<<"'"<<omp_get_thread_num()<<"' i: "<<i<<endl;
		sout<<"j is "<<j<<" t is: ";
		if (v[4]==1){
				v[4]=30;
			}
		
		for (int k=0;k<v.size();k++){
		int t=v[k];
		j+=t;
		// sout<<t<<" ";
		}
		
		
	sout<<"inner "<<j<<endl;
	cout<<sout.str();
	int kk;
	cin>>kk;
		}
		cout<<"over "<<j<<endl;
		
	for (int i=0;i<12;i++){
	  cout<<"vec: "<<v[i]<<endl;
  }
}
