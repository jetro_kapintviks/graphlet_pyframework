import numpy as np
from functools import cmp_to_key
np.random.seed(1)

def save_smaller(nR=600):

	amat=np.loadtxt("PGD-master/data/C500-9.mtx",dtype=int,delimiter=' ',skiprows=2)
	chRow=np.random.choice(amat.shape[0],nR,replace=False)
	# print(len(set(chRow)))
	smallMat=amat[chRow]
	
	listMat=smallMat.flatten()
	maxV=len(set(listMat))
	sInd=1
	
	#print(smallMat)
	vertDik={}
	for ind,i in enumerate(listMat):
		if i not in vertDik:
			vertDik[i]=sInd
			sInd+=1
		smallMat[ind//2,ind%2]=vertDik[i]
	
	# print(smallMat[50:55])
	#print(vertDik)
	# for ind in range(len(smallMat)):
		# row=smallMat[ind]
		# if row[0]>row[1]:
			# smallMat[ind]=row[1],row[0]
	
	# print(smallMat[50:55])
	
	np.savetxt('../test_struct2.csv'.format(nR),
	smallMat,fmt='%3d',delimiter=',')


def save_smaller_multi(nR=600,vertL=None):

	amat=np.loadtxt("PGD-master/data/C500-9.mtx",dtype=int,delimiter=' ',skiprows=2)
	chRow=[]
	for i in range(3):	
		chRow.append(np.random.choice(amat.shape[0],nR,replace=False))
	
	
	# print(len(set(chRow)))
	smallMat=np.zeros((0,3),dtype=int)
	for ind,chR in enumerate(chRow):
		oness=np.ones((nR,1),dtype=int)*ind 
		smallMat=np.vstack((smallMat,
		np.hstack((amat[chR],oness
		)) ))
	
	listMat=smallMat[:,:2].flatten()
	maxV=len(set(listMat))
	sInd=1
	
	#print(smallMat)
	vertDik={}
	for ind,i in enumerate(listMat):
		if vertL is not None:
			vertDik[i]=np.random.choice(vertL)
		elif i not in vertDik and ind<2*nR:
			vertDik[i]=sInd
			sInd+=1
		elif i not in vertDik and  ind>=2*nR:
			i=np.random.choice(list(vertDik.keys()))

		smallMat[ind//2,ind%2]=vertDik[i]
	
	# print(smallMat[50:55])
	#print(vertDik)
	# for ind in range(len(smallMat)):
		# row=smallMat[ind]
		# if row[0]>row[1]:
			# smallMat[ind]=row[1],row[0]
	
	# print(smallMat[50:55])
	
	np.savetxt('test_struct_multi2.csv'.format(nR),
	smallMat,fmt='%3d',delimiter=',')
def compare(one, two):
	if one[0]==two[0]:
		return one[1]-two[1]
	return one[0]-two[0]
	
def read_micro(nR=600):
	amat=np.loadtxt('data/small_{}_mic.csv'.format(nR),dtype=int,delimiter=',',skiprows=1)
	bmat=np.loadtxt('data/small_{}.csv'.format(nR),dtype=int,delimiter=',')
	blis=[(i,j) for i,j in bmat]
	alis=[(i[0],i[1]) for i in amat]
	for r in alis:
		r2=r[1],r[0]
		if r not in blis and r2 in blis:
			blis.remove(r2)
			blis.append(r)
	
	blis.sort(key=cmp_to_key(compare))
	
	alis.sort(key=cmp_to_key(compare))
	
	print(alis[:50],'\n',blis[:50],len(alis),len(blis))
	for r in alis:
		r2=r[1],r[0]
		if r in blis: 
			blis.remove(r)
		elif r2 in blis:
			blis.remove(r2)
		else:
			print('here is, not in otiginal',r)
		
	print('remaining blis ',blis)
	
def check_for_vertI(vertI,nR=600):
    amat=np.loadtxt('data/small_{}_mic.csv'.format(nR),dtype=int,delimiter=',',skiprows=1)
    inds=[]
    for ind,row in enumerate(amat):
        if row[0] in vertI or row[1] in vertI:
            inds.append(ind)
            
    names='src,dst,triangle,2-star,4-clique,4-chordal-cycle,4-tailed-triangle,4-cycle,3-star,4-path'.split(',')
    savM=np.vstack((names,amat[inds,:])) 
    np.savetxt('data/small_{}_vert{}.csv'.format(nR,'_'.join(str(i) for i in vertI  )),
    savM,fmt="%s",delimiter=',')
    print( )
    
    
# 607 for test_struct
# save_smaller(607)
save_smaller_multi(300,np.arange(70,dtype=int))

	
# read_micro(14)

# check_for_vertI([1,2,69,11])
