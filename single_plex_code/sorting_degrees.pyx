"""
The functions needed are:
sortAllC (int[:] edgelist,int[:] degrees,
	int[:] vertInds,bint doParallel)
sortAllPy ( edgelist, degrees,
	vertInds doBucket)
"""

cimport numpy as np
import numpy as np
import time
from cython.parallel import prange
cimport cython
from cpython cimport array
import array
import sys

cdef extern from "alloc_int.c":
	int* alloc_int(int n) nogil

cdef extern from "alloc_int.c":
	void free_int(int* n) nogil

@cython.boundscheck(False)
cdef void sortBatchC( int[:] a,int start,int end,int[:] deg,
		 int[:] newA) nogil:
	cdef int i
	cdef int minDeg=18585682356,maxDeg=0
	for i in range(start,end):
		if deg[a[i]]>maxDeg:
			maxDeg=deg[a[i]]
		if deg[a[i]]<minDeg:
			minDeg=deg[a[i]]
	maxDeg+=1
	cdef int* degreeCnt=alloc_int(maxDeg)
	if not degreeCnt:
		with gil:
			raise MemoryError()
	for i in range(minDeg,maxDeg):
		degreeCnt[i]=0
#	 print 'min/maxDeg',minDeg, maxDeg
	for i in range(start,end):
		degreeCnt[deg[a[i]] ]+=1
	
#	 print 'degree Cnt',degreeCnt,minDeg
	
	cdef int work=0,tempDeg=0
	for i in range(minDeg,maxDeg):
		tempDeg=degreeCnt[i]
		degreeCnt[i]=work
		work+=tempDeg
	   
#	  print 'degree Cnt',degreeCnt,end-start
	for i in range(start,end):
		newA[degreeCnt[ deg[a[i]] ] ]=a[i]
		degreeCnt[ deg[a[i]] ] +=1
	free_int(degreeCnt)
#	 return newA

def sortAllC(a,comp,inds,doParallel):
	# print a.dtype,comp.dtype,inds.dtype,type(len(a))
	return csortAllC(a,comp,inds,len(a),len(inds),doParallel)
@cython.boundscheck(False)
cdef np.ndarray[np.int_t , ndim=1] csortAllC(int[:] a,int[:] comp,
					int[:] inds,int lenA,int lenI,bint doParallel):
	cdef int in_i
	
	cdef int[:] newA=np.zeros((lenA,),dtype=np.int32)
	if doParallel:
		for in_i in prange(lenI-1,nogil=True
						   ,schedule='static'):
			sortBatchC(a,inds[in_i],inds[in_i+1],comp,
			newA[inds[in_i]:inds[in_i+1]  ])
	else:
		for in_i in range(lenI-1):
			sortBatchC(a,inds[in_i],inds[in_i+1],comp,
				newA[inds[in_i]:inds[in_i+1]  ])
#	 print 'here c'
#	 sys.stdout.flush()
	return np.asarray(newA)
def sortBatchPy(baseL,deg,newL):
#	 start,stop=startstop
	
	minD,maxD=8753856835,0
	for i in baseL:
		if minD>deg[i]:
			minD=deg[i]
		if maxD<deg[i]:
			maxD=deg[i]
	maxD+=1
	degCount=np.zeros((maxD,),dtype=np.int32)
	for i in baseL:
		degCount[deg[i]]+=1
	work=0
	for i,x in enumerate(degCount):
		degCount[i],work=work,work+degCount[i]
	
	for i in baseL:
		newL[degCount[deg[i] ]  ]=i
		degCount[deg[i]]+=1
def sortAllPy(a,deg,inds,doBucket=True):
	lenI,lenA=len(inds),len(a)
   
	newA=np.zeros((lenA,),dtype=np.int32)
		
	for in_i in xrange(lenI-1):
		start,end=inds[in_i],inds[in_i+1]
		if doBucket:
			sortBatchPy(a[start:end],deg,
						newA[start:end ])
		else:
			newA[start:end]=sorted(a[start:end],key=lambda x:deg[x])

	return newA

def test_sorting_degrees():
	np.random.seed(1)
	nn=161000

	lenA,forV=nn,nn
	vertI=[]
	while forV>0:
		vI=np.random.randint(1,11)
		if vI>forV:
			vI=forV
		forV-=vI
		vertI.append(vI)
	start=0
	for i,vert in enumerate(vertI):
		vertI[i],start=start,start+vertI[i]
	vertI.append(start) 
	lenI=len(vertI)
	vertI=np.array(vertI)

	a=np.random.randint(0,5,(nn,))
	comp=np.array([5,1,2,3,7])

	print 'a {};\nvertI {}'.format(a[:50],vertI[:50])

	# a,deg,inds
	# sortAllC(int[:] a,int[:] comp,
	# int[:] inds,int lenA,int lenI,bint doParallel):
	stra=time.time()
	acp=None
	acp=sortAllC(a,comp,vertI,True)
	sumCP=(time.time()-stra)

	stra=time.time()
	ac=None
	ac=sortAllC(a,comp,vertI,False)
	sumC=(time.time()-stra)

	stra=time.time()
	apyB=sortAllPy(a,comp,vertI,True)
	sumPyB=(time.time()-stra)

	stra=time.time()
	apy=sortAllPy(a,comp,vertI,False)
	sumPy=(time.time()-stra)


	print 'sample sort:{}'.format(apyB[:50])

	print 'cp_vs_c(0):{}; c_vs_pyb(0):{}; pyb_vs_py(0):{}'.format(
	np.sum([a!=b for a,b in zip(acp,ac)]),
		np.sum([a!=b for a,b in zip(ac,apyB)]),
		np.sum([a!=b for a,b in zip(apyB,apy)]),
	)
	print 'timeC:{}; timeCP:{}; timePyB:{}; timePy:{}'.format(
	sumC,sumCP,sumPyB,sumPy
	)
	print "\n=====================\n"
