
# coding: utf-8

# In[1]:

from Multiplex_Graph import GraphFunc
from mds import cmdscale
import itertools
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pickle
# In[ ]:

import networkx as nx
import numpy as np
from scipy.spatial.distance import euclidean as distance


np.random.seed(8)

def make_powerlaw_graph_corrmatrix(nodes,degree,degree_tri,ind_g=0):
	graph=[]
	for ind,(d,p) in enumerate(zip(degree,degree_tri)):
	    graph.extend([(i,j,ind) for (i,j) in nx.powerlaw_cluster_graph(nodes,d,p,seed=np.random.randint(0,100)).edges()])

	graph_edges=np.array(graph)

	print('made edge list: ',ind_g,flush=True)

	graphF=GraphFunc(doDirected=False)
	graphF.make_graph_table(graph_edges)
	graphF.make_direct_neighbours(subOne=False)# : self.graphNeigh
	print('made neighbor Dikt: ',ind_g,flush=True)

	graphF.make_zero_orbit()
	graphF.count_tri_graphs()# : self.triGNMulti

	print('made orbits: ',ind_g,flush=True)

	orb_mat=graphF.return_orbits_Mat().values
	orb_mat_corr=np.corrcoef(orb_mat.T)

	if np.any(np.isnan(orb_mat_corr)):
		return False,None

	return True,orb_mat_corr

def make_erdos_renyi_graph_corrmatrix(nodes,degree,seed_deg,ind_g=0):
	graph=[]
	for ind,p in enumerate(degree):
	    graph.extend([(i,j,ind) for (i,j) in nx.erdos_renyi_graph(nodes,p,seed=np.random.randint(0,100) ).edges()])

	graph_edges=np.array(graph)
	print(len(graph_edges),seed_deg)

	print('made edge list: ',ind_g,flush=True)

	graphF=GraphFunc(doDirected=False)
	graphF.make_graph_table(graph_edges)
	graphF.make_direct_neighbours(subOne=False)# : self.graphNeigh
	print('made neighbor Dikt: ',ind_g,flush=True)

	graphF.make_zero_orbit()
	graphF.count_tri_graphs()# : self.triGNMulti

	print('made orbits: ',ind_g,flush=True)

	orb_mat=graphF.return_orbits_Mat().values
	orb_mat_corr=np.corrcoef(orb_mat.T)

	if np.any(np.isnan(orb_mat_corr)):
		return False,None

	return True,orb_mat_corr

def make_watts_strogatz_graph_corrmatrix(nodes,k_s,p_s,ind_g=0):
	graph=[]
	for ind,(k,p) in enumerate(zip(k_s,p_s)):
	    print('k,p: ',k,p)
	    graph.extend([(i,j,ind) for (i,j) in nx.watts_strogatz_graph(nodes,k,p,seed=np.random.randint(0,100) ).edges()])

	graph_edges=np.array(graph)

	print('made edge list: ',ind_g,flush=True)

	graphF=GraphFunc(doDirected=False)
	graphF.make_graph_table(graph_edges)
	graphF.make_direct_neighbours(subOne=False)# : self.graphNeigh
	print('made neighbor Dikt: ',ind_g,flush=True)

	graphF.make_zero_orbit()
	graphF.count_tri_graphs()# : self.triGNMulti

	print('made orbits: ',ind_g,flush=True)

	orb_mat=graphF.return_orbits_Mat().values
	orb_mat_corr=np.corrcoef(orb_mat.T)

	if np.any(np.isnan(orb_mat_corr)):
		return False,None

	return True,orb_mat_corr


def make_barabasi_albert_graph_corrmatrix(nodes,m_s,ind_g=0):
	graph=[]
	print(m_s)
	for ind,m in enumerate(m_s):
	    graph.extend([(i,j,ind) for (i,j) in nx.barabasi_albert_graph(nodes,m,seed=np.random.randint(0,100) ).edges()])

	graph_edges=np.array(graph)

	print('made edge list: ',ind_g,flush=True)

	graphF=GraphFunc(doDirected=False)
	graphF.make_graph_table(graph_edges)
	graphF.make_direct_neighbours(subOne=False)# : self.graphNeigh
	print('made neighbor Dikt: ',ind_g,flush=True)

	graphF.make_zero_orbit()
	graphF.count_tri_graphs()# : self.triGNMulti

	print('made orbits: ',ind_g,flush=True)

	orb_mat=graphF.return_orbits_Mat().values
	orb_mat_corr=np.corrcoef(orb_mat.T)

	if np.any(np.isnan(orb_mat_corr)):
		return False,None

	return True,orb_mat_corr

# =======================================================================


nodes=[100,120,150,200,250,300,318,350,400,450,500,550,]

degree=np.array([[8, 15, 16, 21, 24, 42, 44, 44, 55, 57],
[10, 12, 13, 23, 30, 41, 41, 45, 49, 56]]).T

degree_tri=[
[0.5,0.8],
[0.4,0.8],
[0.22,0.8],
[0.4,0.5],
[0.2,0.7],
]

corr_list=np.zeros((0,36,36),dtype=int)
real_ind=0
howManyG=len(nodes)*len(degree)*len(degree_tri)
breakPointG=10

for ind,(n,d,dtri) in zip(range(howManyG),itertools.product(nodes,degree,degree_tri)):
	if real_ind==breakPointG:
		break
	status,corr=make_powerlaw_graph_corrmatrix(n,d,dtri,ind)
	if status:
		corr_list=np.vstack((corr_list,corr.reshape(1, corr.shape[0],-1) ))
		print('Made Graph with nodes: {},degree: {},degree_tri: {} '.format(n,d,dtri))
		real_ind+=1

count_1=real_ind

breakPointG_2=20

seed_er=[10,20,25,30,35,40,45,50,55,60]
degree_er=[
[0.5,0.8],
[0.4,0.8],
[0.22,0.8],
[0.4,0.5],
[0.2,0.7],
]

print(degree_er)


for ind,(n,d,der) in zip(range(howManyG),itertools.product(nodes,seed_er,degree_er)):
	if real_ind==breakPointG_2:
		break
	status,corr=make_erdos_renyi_graph_corrmatrix(n,der,d,real_ind)
	if status:
		corr_list=np.vstack((corr_list,corr.reshape(1, corr.shape[0],-1) ))
		print('Made Graph with nodes: {},seed: {},degree: {} '\
			.format(n,d,der))
		real_ind+=1

count_2=real_ind-count_1


k_sw=np.array([
[8, 15, 16, 21, 24, 42, 44, 44, 55, 57],
[10, 12, 13, 23, 30, 41, 41, 45, 49, 56]]).T

degree_sw=[
[0.5,0.8],
[0.4,0.8],
[0.22,0.8],
[0.4,0.5],
[0.2,0.7],
]
breakPointG_3=30

for ind,(n,k,dsw) in zip(range(howManyG),itertools.product(nodes,k_sw,degree_sw)):
	if real_ind==breakPointG_3:
		break
	status,corr=make_watts_strogatz_graph_corrmatrix(n,k,dsw,real_ind)
	if status:
		corr_list=np.vstack((corr_list,corr.reshape(1, corr.shape[0],-1) ))
		print('Made Graph with nodes: {},k: {},dsw: {} '\
			.format(n,k,dsw))
		real_ind+=1

count_3=real_ind-count_2-count_1

m_ba=np.array([
[8, 15, 16, 21, 24, 42, 44, 44, 55, 57],
[10, 12, 13, 23, 30, 41, 41, 45, 49, 56]]).T

breakPointG_4=40

for ind,(n,m) in zip(range(howManyG),itertools.product(nodes,m_ba)):
	if real_ind==breakPointG_4:
		break
	status,corr=make_barabasi_albert_graph_corrmatrix(n,m,real_ind)
	if status:
		corr_list=np.vstack((corr_list,corr.reshape(1, corr.shape[0],-1) ))
		print('Made Graph with nodes: {},m: {}'\
			.format(n,m))
		real_ind+=1

count_4=real_ind-count_2-count_1-count_3

distance_c=np.zeros((real_ind,real_ind))

for ind,(c1,c2) in enumerate(itertools.combinations(range(corr_list.shape[0]),2)):
	c1t=np.triu(corr_list[c1]).flatten()
	c2t=np.triu(corr_list[c2]).flatten()
	if c1>c2:
		c1,c2=c2,c1

	distance_c[c1,c2]=distance(c1t,c2t)
	distance_c[c2,c1]=distance_c[c1,c2]

Y,evals=cmdscale(distance_c)
Y=Y[:,:3]

print('Y shape: ',Y.shape[0])

cmap_blues=np.vstack((
cm.Blues(np.linspace(0.4,1,count_1)),
cm.Purples(np.linspace(0.4,1,count_2)),
cm.Oranges(np.linspace(0.4,1,count_3)),
cm.Greys(np.linspace(0.4,1,count_4)),
))

with open('saved_graphs_100s.plot','wb') as f:
	pickle.dump((Y,cmap_blues),f)

print('Cmap len: ',cmap_blues.shape[0],count_1,count_2,count_3,count_4)


fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(Y[:,0],Y[:,1],Y[:,2],marker='*',c=cmap_blues)

plt.show()
print('Done',flush=True)



# In[ ]:



