import networkx as nx

import matplotlib.pyplot as plt

nodes=100
degree=7

G=nx.barabasi_albert_graph(nodes,degree,seed=12)

nx.draw(G)
# print(nx.triangles(G))
print(sum(nx.triangles(G).values()))

G=nx.powerlaw_cluster_graph(nodes,degree,0.5,seed=12)

nx.draw(G)
# print(nx.triangles(G))
print(sum(nx.triangles(G).values()))

G=nx.powerlaw_cluster_graph(nodes,degree,1,seed=12)

nx.draw(G)
# print(nx.triangles(G))
print(sum(nx.triangles(G).values()))