import numpy as np
import pandas as pd 

from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.cluster.hierarchy import cophenet
from scipy.spatial.distance import pdist
from generate_synthetic_networks_library import *

def graph_hierarchical_clustering(graph_signature_matrix):
    Z = linkage(graph_signature_matrix, 'ward')
    c, coph_dists = cophenet(Z, pdist(graph_signature_matrix))

    plt.figure(figsize=(25, 10))
    plt.title('Hierarchical Clustering Dendrogram')
    plt.xlabel('nodes')
    plt.ylabel('distance')
    dendrogram(
        Z,
        leaf_rotation=90.,  # rotates the x axis labels
        leaf_font_size=8.,  # font size for the x axis labels
    )
    plt.show()

def read_net_viktor(filename,edge_atr=0):
    raw_graph=np.loadtxt(filename,delimiter=',',skiprows=1,dtype=str)
    conv_graph=np.zeros((0,3),dtype=int)
    convDikt={}
    ind_node=0
    
    for ind,(i,j,k) in enumerate(raw_graph):
        if i not in convDikt:
            convDikt[i]=ind_node
            ind_node+=1
        if j not in convDikt:
            convDikt[j]=ind_node
            ind_node+=1

        conv_graph=np.vstack((conv_graph,(convDikt[i],convDikt[j],edge_atr)))
    
    rev_conv_dikt=pd.DataFrame({'Country' : list(convDikt.keys()),
    							'Index' : list(convDikt.values())})

    return conv_graph,rev_conv_dikt

def make_convdikt_viktor_biplex(filename1,filename2,edge_atr=0):
    raw_graph1=np.loadtxt(filename1,delimiter=',',skiprows=1,dtype=str)
    raw_graph2=np.loadtxt(filename2,delimiter=',',skiprows=1,dtype=str)
    
    conv_graph=np.zeros((0,3),dtype=int)
    
    countries=set(np.concatenate((raw_graph1[:,:2].flatten(),
    						raw_graph2[:,:2].flatten())))
    convDikt={country:ind for ind,country in enumerate(countries)}

    rev_conv_dikt=pd.DataFrame({'Country' : list(convDikt.keys()),
    							'Index' : list(convDikt.values())})

    return rev_conv_dikt
    
    
conv_graph1,rev_conv_dikt1=read_net_viktor('csvs/itn_primary_2000.csv',0)
conv_graph2,rev_conv_dikt2=read_net_viktor('csvs/itn_secondary_2000.csv',1)

conv_graph_biplex=np.vstack((conv_graph1,conv_graph2))

conv_graph_aggregated,rev_conv_dikt_agg=read_net_viktor('csvs/itn_aggregated_2000.csv',0)


draw_correlation_matrix(conv_graph_biplex)

# graph_hierarchical_clustering(conv_graph_biplex)
# graph_hierarchical_clustering(conv_graph_aggregated)

#!!!!!!!! SAVING
# ===========================================

# save_orbits(conv_graph_biplex,'csvs/itn_biplex_orbits.csv')
# save_orbits(conv_graph_aggregated,'csvs/itn_aggregated_orbits.csv')

# rev_conv_dikt_biplex=make_convdikt_viktor_biplex('csvs/itn_primary_2000.csv',
												# 'csvs/itn_secondary_2000.csv')

# rev_conv_dikt_biplex.to_csv('csvs/itn_country_to_index_biplex.csv',index=False)
# rev_conv_dikt_agg.to_csv('csvs/itn_country_to_index_aggregated.csv',index=False)


# neigh_dist,node3_dist=calculate_graph_orbit_summary_distribution(conv_graph_biplex)

# node3_dist=list(node3_dist)
# node3_dist.sort(key= lambda x: x[1])
# node3_dist.sort(key=lambda x: x[0])
# for (i,j,k) in node3_dist:
#     print(i,j,k)


# print(list(neigh_dist),'\n\n========================\n\n',list(node3_dist) )