
import numpy as np
from functools import partial
from generate_synthetic_networks_library import *
import pickle

saved_name='3D_graphs_fourplex_flat.plot'

with open(saved_name,'rb') as f:
	Y,cmap_colors=pickle.load(f)

draw_3D_graphs(Y,cmap_colors,doSave=False,doshow=True,subplot_ind=(1,1,1),fig=None)