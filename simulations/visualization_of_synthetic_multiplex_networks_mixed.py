import itertools
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pickle
import networkx as nx
import numpy as np
import os
import sys

from mpl_toolkits.mplot3d import Axes3D
from scipy.spatial.distance import euclidean as distance

sys.path.append('..')
from multiplex_pygraph.Multiplex_Graph import GraphFunc
from mds import cmdscale

from generate_synthetic_networks_library import * 

nodes=[200]

degrees=np.array([
[14,],
[18, ]]).T

probs=[0.52]

how_many_g=len(probs)*len(degrees)*len(nodes)
big_real_ind=0


# multiplex simulations

# single_size=20
# big_corr_list=np.zeros((3*single_size,36,36),dtype=float)
# for i in range(single_size):
	# print('multiplex ind is: ', i)
	# real_ind,corr_list=run_multiplex_simulations_pl_ba(nodes,degrees,probs,how_many_g,break_point_g=1)
	# big_corr_list[i]=corr_list

	# real_ind,corr_list=run_multiplex_simulations_2ba(nodes,degrees,probs,how_many_g,break_point_g=1)
	# big_corr_list[i+single_size]=corr_list

	# real_ind,corr_list=run_multiplex_simulations_2pl(nodes,degrees,probs,how_many_g,break_point_g=1)
	# big_corr_list[i+(single_size*2)]=corr_list

	# big_real_ind+=1
# cmap_colors=get_colors(3,single_size)

# save_name='saved_graphs_mix_same_conf_multiplex.plot'
# Y=get_3D_projection_from_corrs(big_corr_list)
# draw_3D_graphs(Y,cmap_colors,save_name=save_name)


# ==========================================================================================================================


# singleplex simulations
# single_size=20
# big_corr_list=np.zeros((3*single_size,4,4),dtype=float)
# for i in range(single_size):
# 	print('singleplex ind is: ', i)
# 	real_ind,corr_list=run_multiplex_simulations_singleplex_pl_ba(nodes,degrees,probs,how_many_g,break_point_g=1)
# 	big_corr_list[i]=corr_list

# 	real_ind,corr_list=run_multiplex_simulations_singleplex_2ba(nodes,degrees,probs,how_many_g,break_point_g=1)
# 	big_corr_list[i+single_size]=corr_list

# 	real_ind,corr_list=run_multiplex_simulations_singleplex_2pl(nodes,degrees,probs,how_many_g,break_point_g=1)
# 	big_corr_list[i+(single_size*2)]=corr_list

# 	big_real_ind+=1
# cmap_colors=get_colors(3,single_size)

# save_name='saved_graphs_mix_same_conf_multiplex.plot'
# Y=get_3D_projection_from_corrs(big_corr_list)
# draw_3D_graphs(Y,cmap_colors,save_name=save_name)


# ==========================================================================================================================

# size of cloud
single_size=7*4

single_graph_size=7

big_corr_list=np.zeros((3*single_size,36,36),dtype=float)

nodes=[200,200,200]

degrees=np.array([
[27,76,147],
[27,76,147],]).T

probs=[0.36,0.54,0.83]
shows=[False,False,False]

for ind in range(single_graph_size  ):
	print('multiplex ind is: ', ind)
	i=ind
	real_ind,corr_list=run_multiplex_simulations_cloud_size_2ws(nodes,degrees,probs,how_many_g,break_point_g=3)
	big_corr_list[[ i,i+single_size,i+(single_size*2)  ]]=corr_list
	
	i+=single_graph_size
	real_ind,corr_list=run_multiplex_simulations_cloud_size_2er(nodes,probs,how_many_g,break_point_g=3)
	big_corr_list[ [ i,i+single_size,i+(single_size*2)  ]   ]=corr_list
	
	i+=single_graph_size
	real_ind,corr_list=run_multiplex_simulations_cloud_size_2ba(nodes,degrees,probs,how_many_g,break_point_g=3)
	big_corr_list[ [i,i+(single_size),i+(2*single_size) ]]=corr_list
	
	i+=single_graph_size
	real_ind,corr_list=run_multiplex_simulations_cloud_size_2pl(nodes,degrees,probs,how_many_g,break_point_g=3)
	big_corr_list[  [i,i+(single_size),i+(2*single_size) ]]=corr_list

	big_real_ind+=1

cmap_colors=get_colors(4,single_graph_size)

save_name='saved_graphs_mix_cloudsize_{}.plot'

Y=get_3D_projection_from_corrs(big_corr_list)

# draw_3D_graphs(Y[160:],cmap_colors,save_name=save_name.format(ind),doshow=True)

figs=[ draw_3D_graphs(Y[ind_s:ind_e],cmap_colors,save_name=save_name.format(ind),doshow=show) \
for ind,(ind_s,ind_e,show) in enumerate(zip(range(0,Y.shape[0],single_size), 
	range(single_size,Y.shape[0]+single_size,single_size),shows ) )
	]

plt.show()

# ==========================================================================================================================




