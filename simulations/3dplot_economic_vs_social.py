import numpy as np
import pandas as pd
import os
import re
import time
import sys
import pandas as pd 
import pickle
import random
import matplotlib.pyplot as plt

from functools import partial
from scipy.cluster.hierarchy import dendrogram, linkage,cut_tree
from scipy.cluster.hierarchy import cophenet
from scipy.spatial.distance import pdist
from generate_synthetic_networks_library import *
from scipy.spatial.distance import euclidean as distance




def remove_percent_from_mean(big_corrs,perc_remove=5):
	mean_cor=np.mean(big_corrs,axis=0)
	
	reduced_bymean=np.sum(np.abs(big_corrs-mean_cor).reshape((-1,big_corrs.shape[1]*big_corrs.shape[2])) ,axis=1) 
	
	
	low_bound,high_bound=np.percentile(reduced_bymean,(perc_remove,100-perc_remove))
	
	ret_mask=(reduced_bymean>low_bound) & (reduced_bymean<high_bound)
	
	print('There are now: ',np.sum(ret_mask))
	
	return big_corrs[ret_mask]
	

random.seed(8)
np.random.seed(8)

with open('economic_2_multiple.cors','rb') as f:
	econ_cors=pickle.load(f)

with open('plotting_social.cors','rb') as f:
	soc_cors=pickle.load(f)

	

# with open('3D_graphs_biplex_many.corr_list','rb') as f:
	# synth_cors=pickle.load(f)

# print('econ size: {}, social size: {}'.format(len(econ_cors['indivudual_class_combinations_2']),len(soc_cors['indivudual_social_class_combinations_2'] )))

# sys.exit()

# soc_sumcor=soc_cors['summary_by_class_comb_combination'][0].astype(float)
# soc_sumcor=soc_sumcor/soc_sumcor[0,0]

# econ_sumcor=econ_cors['summary_by_class_comb_combination'][0].astype(float)
# econ_sumcor=econ_sumcor/econ_sumcor[0,0]

# econ_cors=np.array(econ_cors['indivudual_class_combinations_2'])
# soc_cors=np.array(soc_cors['indivudual_social_class_combinations_2'])



econ_cors=econ_cors[:1000]
soc_cors=soc_cors[:1000]



# econ_cors=remove_percent_from_mean(econ_cors,1)

# soc_cors=remove_percent_from_mean(soc_cors,1)

# econ_cors=np.zeros((0,36,36))
# soc_cors=np.zeros((0,36,36))

synth_cors=np.zeros((0,36,36))

save_graph_name='economic_vs_social_vs_synth.plot'
doSave=True
test_colors=False
n_synths=synth_cors.shape[0]//4

corr_list=np.zeros(( econ_cors.shape[0]+soc_cors.shape[0]+synth_cors.shape[0]  ,36,36))
corr_list[:econ_cors.shape[0]]=econ_cors
corr_list[econ_cors.shape[0]:econ_cors.shape[0]+soc_cors.shape[0] ]=soc_cors

corr_list[econ_cors.shape[0]+soc_cors.shape[0]:]=synth_cors




# cmap_colors=np.vstack((
# cm.Blues(np.linspace(0.4,1,econ_cors.shape[0])),

# cm.Oranges(np.linspace(0.4,1,soc_cors.shape[0] )),

# ))


cmap_colors=np.vstack((

cm.Greens(np.linspace(0.4,1,econ_cors.shape[0])),
cm.cool(np.linspace(0.0,0.5,soc_cors.shape[0])),

cm.Blues(np.linspace(0.4,1,n_synths)),
cm.Purples(np.linspace(0.4,1,n_synths)),
cm.Oranges(np.linspace(0.4,1,n_synths)),
cm.Greys(np.linspace(0.4,1,n_synths)),


))











# print('econ sum cor',econ_sumcor.shape,econ_cors[0][:5,:5])

# for indec in range(econ_cors.shape[0]):
	# econ_cors[indec]=np.abs((econ_cors[indec].astype(float) /econ_cors[indec][0,0])-econ_sumcor)
	
	
	
# for sc in soc_cors:
	# sc=np.abs((sc/sc[0,0])-soc_sumcor)
	


print('corr list shape: ',corr_list.shape)

distance_c=np.zeros((corr_list.shape[0],corr_list.shape[0]))
for ind,(c1,c2) in enumerate(itertools.combinations(range(corr_list.shape[0]),2)):
	c1t=np.triu(corr_list[c1]).flatten()
	c2t=np.triu(corr_list[c2]).flatten()
	if c1>c2:
		c1,c2=c2,c1

	distance_c[c1,c2]=distance(c1t,c2t)
	distance_c[c2,c1]=distance_c[c1,c2]

# from the upper triangle of the distance matrix with multidimensional scaling 
Y,evals=cmdscale(distance_c)
Y=Y[:,:3]

print('Y shape: ',Y.shape)







print('Color Shape: ',cmap_colors.shape)



draw_3D_graphs(Y,cmap_colors,save_name=save_graph_name,doSave=doSave,doshow=True,subplot_ind=(1,1,1),fig=None,graphShape='o',starSize=20)