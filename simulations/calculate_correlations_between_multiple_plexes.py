import numpy as np
import pandas as pd 

from scipy.cluster.hierarchy import dendrogram, linkage,cut_tree
from scipy.cluster.hierarchy import cophenet
from scipy.spatial.distance import pdist
from generate_synthetic_networks_library import *
from scipy.spatial.distance import euclidean as distance



def graph_hierarchical_clustering(graph_signature_matrix,layerD,doSave=False):
    Z = linkage(graph_signature_matrix, 'ward')
    c, coph_dists = cophenet(Z, pdist(graph_signature_matrix))

    plt.figure(figsize=(25, 10))
    plt.title('Hierarchical Clustering Dendrogram')
    plt.xlabel('nodes')
    plt.ylabel('distance')
    dendrogram(
        Z,
        leaf_rotation=90.,  # rotates the x axis labels
        leaf_font_size=8.,  # font size for the x axis labels
    )
    plt.show()

    if doSave==True:
	    clus_list=np.array(cut_tree(Z,n_clusters=[11]))

	    np.savetxt('csvs/clusters_itn_4digit_c.csv', clus_list )
	    np.savetxt('csvs/clusters_itn_4digit_l.csv', np.array([ layerD[i] for i in range(graph_signature_matrix.shape[0]) ]) )


def read_multiplex_net_viktor(filename,edge_atr=0):
	raw_graph=pd.read_csv(filename,delimiter=',',skiprows=0)

	howManyReal=np.sum(raw_graph['Layer']%10!=0)


	conv_graph=np.zeros((howManyReal,3),dtype=int)
	convDikt={}
	ind_node=0
	ind=0

	convEDikt={}
	ind_layer=0
	for (indxx,i,j,k,l) in raw_graph.itertuples() :
		if l%10==0:
			continue

		if i not in convDikt:
			convDikt[i]=ind_node
			ind_node+=1
		if j not in convDikt:
			convDikt[j]=ind_node
			ind_node+=1
		if l not in convEDikt:
			convEDikt[l]=ind_layer
			ind_layer+=1

		conv_graph[ind]=(convDikt[i],convDikt[j],convEDikt[l] )
		ind+=1
	
	rev_conv_dikt=pd.DataFrame({'Country' : list(convDikt.keys()),
								'Index' : list(convDikt.values())})

	rev_conv_layer_dikt={convEDikt[k]:k for k in convEDikt}

	return conv_graph,rev_conv_dikt,rev_conv_layer_dikt


def get_distance_mat(big_corr_list):
	distance_c=np.zeros((big_corr_list.shape[0],big_corr_list.shape[0]))

	for ind,(c1,c2) in enumerate(itertools.combinations(range(big_corr_list.shape[0]),2)):
		c1t=np.triu(big_corr_list[c1]).flatten()
		c2t=np.triu(big_corr_list[c2]).flatten()
		if c1>c2:
			c1,c2=c2,c1

		distance_c[c1,c2]=distance(c1t,c2t)
		distance_c[c2,c1]=distance_c[c1,c2]

	return distance_c


graph_edges,conv_g,conv_edges=read_multiplex_net_viktor('csvs/4digit_ITN.csv')


print (graph_edges,len(set(graph_edges[:,2])),  )


cors=np.zeros((0,4,4))

for l in range(len(conv_edges)):
	temp_edges=graph_edges[graph_edges[:,2]==l ]
	temp_edges[:,2]=0
	status,oneM=calculate_corrmatrix(temp_edges)
	cors=np.vstack((cors,oneM.reshape(1,oneM.shape[0],-1  ) ))

dist_mat=get_distance_mat(cors)



graph_hierarchical_clustering(dist_mat,conv_edges)