'''

'''
import numpy as np
from functools import partial
from generate_synthetic_networks_library import *

def iterate_multiple_configs(calc_synt_corrs,synt_type,nodes,probs,n_instances,corr_list):
	
	print('Calculating for SyntType: ',synt_type)
	for i in range(n_instances):
		print('instance num: ',i,n_instances)
		
		real_ind,inner_corr_list = calc_synt_corrs[synt_type](*(nodes,probs))	

		corr_list=np.vstack((corr_list,inner_corr_list))
			

	return corr_list


nodes=np.linspace(100,500,5,dtype=int)#[100,150,200,250,300,350]#[4:5]



probs=np.linspace(0.2,0.8,5)#[0.2,0.35,0.5,0.75,0.8]#[4:]

tri_prob=0.8
ws_prob=0.01

# degrees=np.array([
# degrees_t
# [10, 23, 36, 41, 53]
# ]).T
corr_dim={1:4,2:36,3:280,4:2160,5:5544}
test_colors=False
n_instances=20
n_configs=len(nodes)*len(probs)
howManyFlatten=2
howManyPlex=None

howManyG=n_configs#len(nodes)*len(degrees)*len(degrees_tri)

calc_synt_corrs={
'erdos_renyi' : partial(run_multiplex_simulations_2er_new,how_many_g=howManyG,break_point_g=n_configs),
'watts_strogatz' : partial(run_multiplex_simulations_2ws_new,ws_prob=ws_prob,how_many_g=howManyG,break_point_g=n_configs),
'barabasi_albert' : partial(run_multiplex_simulations_2ba_new,how_many_g=howManyG,break_point_g=n_configs),
'power_law': partial(run_multiplex_simulations_2pl_new,tri_prob=tri_prob,how_many_g=howManyG,break_point_g=n_configs),
}



calc_synt_corrs1={
'erdos_renyi' : partial(run_singleplex_simulations_er,how_many_g=howManyG,break_point_g=n_configs),
'watts_strogatz' : partial(run_singleplex_simulations_ws,ws_prob=ws_prob,how_many_g=howManyG,break_point_g=n_configs),
'barabasi_albert' : partial(run_singleplex_simulations_ba,how_many_g=howManyG,break_point_g=n_configs),
'power_law': partial(run_singleplex_simulations_pl,tri_prob=tri_prob,how_many_g=howManyG,break_point_g=n_configs),
}
sorted_synths=sorted(list(calc_synt_corrs.keys()))

calc_synt_corrs_more={
'erdos_renyi' : partial(run_multiplex_simulations_more_er,how_many_g=howManyG,numPlex=howManyPlex,break_point_g=n_configs),
'watts_strogatz' : partial(run_multiplex_simulations_more_ws,ws_prob=ws_prob,numPlex=howManyPlex,how_many_g=howManyG,break_point_g=n_configs),
'barabasi_albert' : partial(run_multiplex_simulations_more_ba,numPlex=howManyPlex,how_many_g=howManyG,break_point_g=n_configs),
'power_law': partial(run_multiplex_simulations_more_pl,numPlex=howManyPlex,tri_prob=tri_prob,how_many_g=howManyG,break_point_g=n_configs),
}

calc_synt_corrs_flat={
'erdos_renyi' : partial(run_multiplex_simulations_singleplex_pl,howManyPlexes=howManyFlatten, how_many_g=howManyG,break_point_g=n_configs),
'watts_strogatz' : partial(run_multiplex_simulations_singleplex_ws,howManyPlexes=howManyFlatten, ws_prob=ws_prob,how_many_g=howManyG,break_point_g=n_configs),
'barabasi_albert' : partial(run_multiplex_simulations_singleplex_ba,howManyPlexes=howManyFlatten,how_many_g=howManyG,break_point_g=n_configs),
'power_law': partial(run_multiplex_simulations_singleplex_pl,howManyPlexes=howManyFlatten,tri_prob=tri_prob,how_many_g=howManyG,break_point_g=n_configs),
}




save_graph_name='reses/3D_graphs_biplex_2000_flat.plot'
doSave=True

corr_list=np.zeros((0,corr_dim[1],corr_dim[1] ))
for synt_type in sorted_synths:
	corr_list=iterate_multiple_configs(calc_synt_corrs_flat,synt_type,nodes,probs,n_instances,corr_list)



# corr_list=np.zeros((0,36,36))
# for synt_type in sorted_synths:
	# corr_list=iterate_multiple_configs(calc_synt_corrs,synt_type,nodes,probs,n_instances,corr_list)



# corr_list=np.zeros((0,280,280))
# for synt_type in sorted_synths:
	# corr_list=iterate_multiple_configs(calc_synt_corrs3,synt_type,nodes,probs,n_instances,corr_list)

	
if doSave:
	with open(save_graph_name[:save_graph_name.rindex('.')]+'.corr_list','wb') as f:
		pickle.dump(corr_list,f)
	
	
	
# get distance matrix for all graphs that were generated 
distance_c=np.zeros((corr_list.shape[0],corr_list.shape[0]))
for ind,(c1,c2) in enumerate(itertools.combinations(range(corr_list.shape[0]),2)):
	c1t=np.triu(corr_list[c1]).flatten()
	c2t=np.triu(corr_list[c2]).flatten()
	if c1>c2:
		c1,c2=c2,c1

	distance_c[c1,c2]=distance(c1t,c2t)
	distance_c[c2,c1]=distance_c[c1,c2]

# from the upper triangle of the distance matrix with multidimensional scaling 
Y,evals=cmdscale(distance_c)
Y=Y[:,:3]

print('Y shape: ',Y.shape[0])

cmap_colors=np.vstack((
cm.Blues(np.linspace(0.4,1,n_instances*n_configs)),
cm.Purples(np.linspace(0.4,1,n_instances*n_configs)),
cm.Oranges(np.linspace(0.4,1,n_instances*n_configs)),
cm.Greys(np.linspace(0.4,1,n_instances*n_configs)),
))

if test_colors:
	poses=np.arange(cmap_colors.shape[0]*2).reshape((cmap_colors.shape[0],2))
	plt.figure(figsize=(20,20))
	plt.scatter(poses[:,0],poses[:,1],c=cmap_colors)
	plt.savefig('test_colors.pdf')
	plt.close()
	
print('Color Shape: ',cmap_colors.shape)
print(sorted_synths)


draw_3D_graphs(Y,cmap_colors,save_name=save_graph_name,doSave=doSave,doshow=True,subplot_ind=(1,1,1),fig=None,graphShape='o',starSize=30)