import numpy as np
import pandas as pd 

from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.cluster.hierarchy import cophenet
from scipy.spatial.distance import pdist
from generate_synthetic_networks_library import *

def read_brain_net(filename,edge_atr=0):
    raw_graph=np.loadtxt(filename,delimiter=',',skiprows=1,dtype=str)
    conv_graph=np.zeros((0,3),dtype=int)
    convDikt={}
    ind_node=0
    
    for ind,(i,j,k) in enumerate(raw_graph):
        if i not in convDikt:
            convDikt[i]=ind_node
            ind_node+=1
        if j not in convDikt:
            convDikt[j]=ind_node
            ind_node+=1

        conv_graph=np.vstack((conv_graph,(convDikt[i],convDikt[j],edge_atr)))
   
    return conv_graph

graph_edges_struct=read_brain_net('csvs/3011_structural.csv',edge_atr=0)
graph_edges_func=read_brain_net('csvs/3011_functional.csv',edge_atr=1)
graph_edges=np.vstack((graph_edges_func,graph_edges_struct))

draw_correlation_matrix(graph_edges)

