import itertools
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pickle
import networkx as nx
import numpy as np
import os

from mpl_toolkits.mplot3d import Axes3D
from scipy.spatial.distance import euclidean as distance
from scipy.spatial.distance import cosine

from Multiplex_Graph import GraphFunc
from mds import cmdscale
from generate_synthetic_networks_library import *


def social_networks_analysis():
	graph_edges=np.loadtxt('multiplex_data/CS-Aarhus_multiplex.edges',delimiter=',')
	edge_types=set(graph_edges[:,2])
	# print('edge types',edge_types)
	# input()
	pairwise1_corrmatrices=np.zeros((0,4,4))

	for ind,(plex_1,) in enumerate(itertools.combinations(edge_types,1)):
		pairwise_edges=graph_edges[(graph_edges[:,2]==plex_1)]
		pairwise_edges[pairwise_edges[:,2]==plex_1,2]=0

		status,pairwise_corrmatrix=calculate_corrmatrix(pairwise_edges)
		print(pairwise_corrmatrix[:5,:5])
		pairwise1_corrmatrices=np.vstack((pairwise1_corrmatrices,pairwise_corrmatrix.reshape(1,pairwise_corrmatrix.shape[0],-1)  ))

	Y1=get_3D_projection_from_corrs(pairwise1_corrmatrices)

	pairwise2_corrmatrices=np.zeros((0,36,36))

	for ind,(plex_1,plex_2) in enumerate(itertools.combinations(edge_types,2)):
		pairwise_edges=graph_edges[(graph_edges[:,2]==plex_1) |(graph_edges[:,2]==plex_2)]
		pairwise_edges[pairwise_edges[:,2]==plex_1,2]=0
		pairwise_edges[pairwise_edges[:,2]==plex_2,2]=1

		status,pairwise_corrmatrix=calculate_corrmatrix(pairwise_edges)
		print(pairwise_corrmatrix.shape)
		pairwise2_corrmatrices=np.vstack((pairwise2_corrmatrices,pairwise_corrmatrix.reshape(1,pairwise_corrmatrix.shape[0],-1)  ))

	Y2=get_3D_projection_from_corrs(pairwise2_corrmatrices)

	draw_correlation_matrix(pairwise_edges)

	pairwise3_corrmatrices=np.zeros((0,280,280))
	for ind,(plex_1,plex_2,plex_3) in enumerate(itertools.combinations(edge_types,3)):
		pairwise_edges=graph_edges[(graph_edges[:,2]==plex_1) |(graph_edges[:,2]==plex_2) |(graph_edges[:,2]==plex_3)]
		pairwise_edges[pairwise_edges[:,2]==plex_1,2]=0
		pairwise_edges[pairwise_edges[:,2]==plex_2,2]=1
		pairwise_edges[pairwise_edges[:,2]==plex_3,2]=2

		status,pairwise_corrmatrix=calculate_corrmatrix(pairwise_edges)
		# print(pairwise_corrmatrix[:5,:5])
		pairwise3_corrmatrices=np.vstack((pairwise3_corrmatrices,pairwise_corrmatrix.reshape(1,pairwise_corrmatrix.shape[0],-1)  ))

	Y3=get_3D_projection_from_corrs(pairwise3_corrmatrices)

	pairwise4_corrmatrices=np.zeros((0,2160,2160))
	for ind,(plex_1,plex_2,plex_3,plex_4) in enumerate(itertools.combinations(edge_types,4)):
		pairwise_edges=graph_edges[(graph_edges[:,2]==plex_1) |(graph_edges[:,2]==plex_2) |(graph_edges[:,2]==plex_3)|(graph_edges[:,2]==plex_4)]
		pairwise_edges[pairwise_edges[:,2]==plex_1,2]=0
		pairwise_edges[pairwise_edges[:,2]==plex_2,2]=1
		pairwise_edges[pairwise_edges[:,2]==plex_3,2]=2
		pairwise_edges[pairwise_edges[:,2]==plex_4,2]=3

		status,pairwise_corrmatrix=calculate_corrmatrix(pairwise_edges)
		# print(pairwise_edges)
		pairwise4_corrmatrices=np.vstack((pairwise4_corrmatrices,pairwise_corrmatrix.reshape(1,pairwise_corrmatrix.shape[0],-1)  ))

	Y4=get_3D_projection_from_corrs(pairwise4_corrmatrices)

	cmap_colors=np.vstack((
		cm.Oranges(np.linspace(0.4,1,5)),
		cm.Greens(np.linspace(0.4,1,10)),
		cm.Greys(np.linspace(0.4,1,10)),
		cm.Purples(np.linspace(0.4,1,5)),
		))
	
	# for i in (Y1,Y2,Y3,Y4):
	# 	print (i.shape)
	# fig=draw_3D_graphs(np.vstack((Y1,Y2,Y3,Y4)),cmap_colors,save_name='3D_plex_combinations_to4.plot',doshow=True,subplot_ind=(1,1,1))

	# stat,whole_corr=calculate_corrmatrix(graph_edges,subOne=True)
	# print('!!!! whole corr shape',whole_corr.shape)
	# Y=get_3D_projection_from_corrs(whole_corr)
	# cmap_colors=get_colors(1,1)
	# fig=draw_3D_graphs(Y,cmap_colors,save_name='3D_social_networks_all.plot',doshow=True,subplot_ind=(2,1,2),fig=fig)


social_networks_analysis()
neigh_dist,node3_dist=calculate_graph_orbit_summary_distribution(conv_graph_biplex)

