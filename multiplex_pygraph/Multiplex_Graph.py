
# coding: utf-8

# In[105]:

import numpy as np
import itertools
import math
import pandas as pd


class GraphFunc:
    """
    GraphFunc To calculate multiplex graphlet orbitals:
    general flow is:
    graphF=GraphFunc(doDirected=False)
    graphF.load_file('london_transport_multiplex.csv',delimiter=',',skiprows=0)
    graphF.make_direct_neighbours(subOne=True) : self.graphNeigh
    graphF.make_zero_orbit()
    graphF.count_tri_graphs() : self.triGNMulti
    graphF.return_orbits_Mat()
    graphF.save_orbits('london_multi2.csv')

    ----------
    From edge list
    ----------
    graphF=GraphFunc(doDirected=False)
    graphF.make_graph_table(graph_edges)
    graphF.make_direct_neighbours(subOne=False)# : self.graphNeigh

    graphF.make_zero_orbit()
    graphF.count_tri_graphs()# : self.triGNMulti

    orb_mat=graphF.return_orbits_Mat().values


    """
    def __init__(self,doDirected=False):
        self.doDirected=doDirected
        self.factorialDikt={}
        self.combR2Dikt={}
        pass
    def load_file(self,inputfile,delimiter=',',dtype=np.int32,**kwargs):
        self.inputfile=inputfile
        
        graph_table=np.loadtxt(inputfile,delimiter=delimiter,dtype=dtype,
                    **kwargs)
        return self.make_graph_table(graph_table)

    def make_graph_table(self,graph_table):
        fix_graph_table=np.zeros((len(graph_table),3)
                ,dtype=np.int32)
        curVInd=0
        vertDikt={}
        for ind,(i,j,k) in enumerate(graph_table):
            if i not in vertDikt:
                vertDikt[i]=curVInd
                curVInd+=1
            if j not in vertDikt:
                vertDikt[j]=curVInd
                curVInd+=1
            
            if vertDikt[i]!=vertDikt[j]:
                fix_graph_table[ind]=(vertDikt[i],vertDikt[j],k)


        self.vertN=curVInd
        self.graph_t=fix_graph_table
        self.edgeN=(2**len(set(fix_graph_table[:,2])))-1
        self.vertDikt=vertDikt
        self.revvertDikt={vertDikt[i]:i for i in vertDikt}

        return fix_graph_table,curVInd
    def load_edge_list(self,edgeLists):
        graph_table=[]
        for k,eL in enumerate(edgeLists):
            graph_table.extend([(i,j,k) for (i,j) in eL])
            
        fix_graph_table=np.zeros((len(graph_table),3)
                ,dtype=np.int32)
        curVInd=0
        vertDikt={}
        for ind,(i,j,k) in enumerate(graph_table):
            if i not in vertDikt:
                vertDikt[i]=curVInd
                curVInd+=1
            if j not in vertDikt:
                vertDikt[j]=curVInd
                curVInd+=1
            
            if vertDikt[i]!=vertDikt[j]:
                fix_graph_table[ind]=(vertDikt[i],vertDikt[j],k)


        self.vertN=curVInd
        self.graph_t=fix_graph_table
        self.edgeN=(2**len(set(fix_graph_table[:,2])))-1
        self.vertDikt=vertDikt

        self.revvertDikt={vertDikt[i]:i for i in vertDikt}
        
        return fix_graph_table,curVInd
    def make_direct_neighbours(self,subOne=False,do_plex_wrap=True):
        graphHeigh=[{} for i in  range(self.vertN)]

        
        mMax=-1

        if do_plex_wrap:
            self.edgeN=(2**len(set(self.graph_t[:,2])))-1
            self.edgeN_nonF=len(set(self.graph_t[:,2]))
        else:
            self.edgeN=len(set(self.graph_t[:,2]))
            self.edgeN_nonF=int(math.log2(self.edgeN1) )


        #!!!!  edgeN ke e 3 bidejki e so directed
        if self.doDirected: self.edgeN=3
        

        for u,v,k in self.graph_t:
            if subOne: k-=1
            k1=k
            
            if k<2 and self.doDirected:
                k2=1-k1
            else:
                k2=k
            if v in graphHeigh[u]:
                lis=self.get_all_comb(self.edgeN_nonF,graphHeigh[u][v])
                lis=list(lis)
                lis.append(k1)
                lis.sort()
                # print(lis,self.edgeN_nonF,self.get_m_list(self.edgeN_nonF,lis))
                m=self.get_m_list(self.edgeN_nonF,lis)
                graphHeigh[u][v]=m
                # if m==3: print('im here',v,u,)

                if mMax<m: mMax=m
            else:
                # if k1==0: print('im here',v,u,)

                graphHeigh[u][v]=k1
            if u in graphHeigh[v]:
                lis=self.get_all_comb(self.edgeN_nonF,graphHeigh[v][u])
                lis=list(lis)
                lis.append(k2)
                lis.sort()
                # print(lis,self.edgeN_nonF,self.get_m_list(self.edgeN_nonF,lis))
                m=self.get_m_list(self.edgeN_nonF,lis)
                # if m==3: print('im here',v,u,)
                graphHeigh[v][u]=m
                if mMax<m: mMax=m
            else:
                # if k2==0: print('im here',v,u,)
                graphHeigh[v][u]=k2
            
            
            
        
#         if mMax!=-1: 
#             print()
#             self.edgeN
        self.graphNeigh=graphHeigh

    def count_tri_graphs(self):
        triGN=np.zeros((self.vertN,3), dtype=int)
        triGNMulti=[]
        for ind in range(3):
            triGNMulti.append([{} for i in  range(self.vertN)])
        
        
        for vind,vDikt in enumerate(self.graphNeigh):
            runnedThrough=set([])
            vind_set=set([vind])
            for k,j in itertools.combinations(vDikt.keys(),2):
                if k in self.graphNeigh[j]:
                    triGN[vind][2]+=1
                    
                    #Multiplex======================
                    indComb=self.calc_combR2(vDikt[k],vDikt[j])
                    indComb+=self.graphNeigh[j][k]*self.comb2_num()
                    triGNMulti[2][vind][indComb]=triGNMulti[2][vind].get(indComb,0)+1
                    
                else:
                    triGN[vind][1]+=1
                    
                    #Multiplex======================
                    indComb=self.calc_combR2(vDikt[k],vDikt[j])
                    triGNMulti[1][vind][indComb]=triGNMulti[1][vind].get(indComb,0)+1
                    
                    
                if k not in runnedThrough:
                    setK_diff=self.graphNeigh[k].keys() - vDikt.keys()-vind_set
                    triGN[vind][0]+=len(setK_diff)-1
                    runnedThrough.add(k)
                    
                    #Multiplex======================
                    indComb_base=vDikt[k]*self.edgeN
                    
                    for ik in setK_diff:
                        # print(vind,k,ik,vDikt)

                        indComb=indComb_base+ self.graphNeigh[k][ik]
#                         indComb=(vDikt[k],self.graphNeigh[k][ik])
                        triGNMulti[0][vind][indComb]=triGNMulti[0][vind].get(indComb,0)+1
                    
                if j not in runnedThrough:
                    setJ_diff=self.graphNeigh[j].keys() - vDikt.keys()-vind_set
                    
                    triGN[vind][0]+=len(setJ_diff)-1
                    runnedThrough.add(j)
                    
                    #Multiplex========================================
                    indComb_base=vDikt[j]*self.edgeN
                    
                    for ij in setJ_diff:
                        # print(vind,j,ij,vDikt)

                        indComb=indComb_base+ self.graphNeigh[j][ij]
                        
#                         indComb=(vDikt[j],self.graphNeigh[j][ij])
                        triGNMulti[0][vind][indComb]=triGNMulti[0][vind].get(indComb,0)+1
            
        self.triGN=triGN
        self.triGNMulti=triGNMulti
        return triGN
    def calc_combR2(self,a,b):
        if (a,b) in self.combR2Dikt:
            return self.combR2Dikt[(a,b)]
        if a>b:
            a,b=b,a
            
        row_n=self.sumrowsR(self.edgeN-a+1)
        combr2_ret=int(row_n+(b-a))

        if (a,b) not in self.combR2Dikt:
            self.combR2Dikt[(a,b)]=combr2_ret 

        return combr2_ret
    def show_iters(self):
        pass
    def sumrowsR(self,a):
        en=self.edgeN
        if en==a:
            return a
        return int((en+a)*(en-a+1)/2)
    def comb2_num(self):
        n=self.edgeN+1
        k=2
        return self.calc_binom(n,k)
    def calc_binom(self,n,k):
        if  (n,k) in self.factorialDikt:
            return self.factorialDikt[(n,k)]

        self.factorialDikt[(n,k)]=math.factorial(n)\
        //(math.factorial(n-k)*math.factorial(k))

        return self.factorialDikt[(n,k)]

    def return_orbits_Dikt(self):
        dikt_orbits={i:[1]*3 for i in self.vertDikt}
        
        for indG in range(3):
            for ind in self.vertDikt:
                dikt_orbits[ind][indG]= self.triGNMulti[indG][ self.vertDikt[ind] ]
        
        return dikt_orbits
    def make_zero_orbit(self):
        self.zeroGCMulti=np.zeros((self.vertN,self.edgeN),dtype=int)
        for v,vD in enumerate(self.graphNeigh):
            try:
                countsV=np.bincount(list(vD.values()))
            except:
                print(countsV,vD.values())
                sys.exit()
            # print('counts shape ',v,vD,countsV.shape[0])
            self.zeroGCMulti[v,:countsV.shape[0]]=countsV
            
    def return_full_orbit_counts(self):
        wedgepN=self.edgeN**2
        wedgesN=self.comb2_num()
        triN=self.comb2_num()*self.edgeN
        return self.edgeN,wedgepN,wedgesN,triN
    def return_orbits_Mat(self):
        # 9+6+18 za tri plexa
        wedgepN=self.edgeN**2
        wedgesN=self.comb2_num()
        triN=self.comb2_num()*self.edgeN
        sumOrb=wedgepN+wedgesN+triN+self.edgeN
        # print('sumorbs',wedgepN,wedgesN,triN,self.edgeN,self.zeroGCMulti.shape)
        orbs_indexes=np.repeat(np.arange(4),(self.edgeN,wedgepN,wedgesN,triN))
        orbs_start0=np.repeat((0,self.edgeN,wedgepN+self.edgeN,wedgesN+wedgepN+self.edgeN)\
            ,(self.edgeN,wedgepN,wedgesN,triN))
        # print(orbs_start0[:10])
        starting_mat_orbs=np.hstack((self.zeroGCMulti,            np.zeros((self.vertN,sumOrb-self.edgeN),dtype=int)  ))
        
        mat_orbits=pd.DataFrame(starting_mat_orbs,            index=self.vertDikt.keys(),
                columns=['orbital_[{},{}]'.format(i,j-k) for i,j,k in zip(orbs_indexes,range(sumOrb),orbs_start0)  ]   )
        
        count_start=[self.edgeN,self.edgeN+wedgepN,self.edgeN+wedgepN+wedgesN]
        for indG,cS in zip(range(3),count_start):
            
            for ind in self.vertDikt:
                ind_dikt=self.triGNMulti[indG][self.vertDikt[ind]]
                for ind_col in ind_dikt:
                    mat_orbits.ix[ind,ind_col+cS  ]=ind_dikt[ind_col]
        
        return mat_orbits

    def return_orbits_fast(self,internal_node_id=True):
        
        if not internal_node_id:
            raise Exception("Can't return numpy array with node ids outside of [0,n-1] range")

        wedgepN=self.edgeN**2
        wedgesN=self.comb2_num()
        triN=self.comb2_num()*self.edgeN
        sumOrb=wedgepN+wedgesN+triN+self.edgeN

        mat_orbits=np.zeros((self.vertN,sumOrb),dtype=int)
        # print('self vertdikt')

        mat_orbits[:,:self.edgeN]=self.zeroGCMulti

        count_start=[self.edgeN,self.edgeN+wedgepN,self.edgeN+wedgepN+wedgesN]

        for indG,cS in zip(range(3),count_start):     
            for ind in self.vertDikt:
                ind_dikt=self.triGNMulti[indG][self.vertDikt[ind]]
                for ind_col in ind_dikt:
                    if internal_node_id:
                        mat_orbits[ self.vertDikt[ind],ind_col+cS  ]=ind_dikt[ind_col]
        
        return mat_orbits


    def return_orbits_dikt(self):
        

        wedgepN=self.edgeN**2
        wedgesN=self.comb2_num()
        triN=self.comb2_num()*self.edgeN
        sumOrb=wedgepN+wedgesN+triN+self.edgeN

        dikt_orbits={ind:{} for ind in self.vertDikt}
        # print('self vertdikt')

        for ind in self.vertDikt:
            zeroG=self.zeroGCMulti[self.vertDikt[ind]]
            for conv_ind,conv_val in enumerate(zeroG):
                if conv_val!=0:
                    dikt_orbits[ind][conv_ind]=conv_val

            # print ('ind is ',ind,'dikt is ',self.graphNeigh[self.vertDikt[ind]] )


        count_start=[self.edgeN,self.edgeN+wedgepN,self.edgeN+wedgepN+wedgesN]
        for indG,cS in zip(range(3),count_start):
            
            for ind in self.vertDikt:
                ind_dikt=self.triGNMulti[indG][self.vertDikt[ind]]
                for ind_col in ind_dikt:
                    dikt_orbits[ind][ind_col+cS  ]=ind_dikt[ind_col]
        
        return dikt_orbits

    def save_orbits(self,filename):
        d_orbs=self.return_orbits_Mat()
        d_orbs.to_csv(filename)
        
    def get_combs(self,n,k):

        return self.calc_binom(n,k)

    def get_sum(self,c,i):
        return (c*i)-(i*(i+1))//2

    def get_comb2(self,c,m):
        if m>=self.get_combs(c,2):
            return
        root=np.roots([1,(1-(2*c)),2*m])
        row=min(root)
        row=int(round(row,4))
        col=m-self.get_sum(c,row)
        return row,col+row+1

    def get_combs_k(self,c,k,m):
        if k==2:
            return self.get_comb2(c,m)
        step_one=-1
        while m>=0:
            try:
                step_one+=1
                m-=self.get_combs(c-1-step_one,k-1)
            except:
                print('Error in get combs',c,k,m,step_one)
                sys.exit()
        m+=self.get_combs(c-1-step_one,k-1)
        retL=[step_one]
        lis=[i+1+step_one for i in self.get_combs_k(c-1-step_one,k-1,m)]
        retL.extend(lis)
        return retL

    def get_all_comb(self,c,m):
        if m+1>=2**c:
            return
        combs_k=1
        while combs_k<=c:
            if m<0:
                break
            m-=self.get_combs(c,combs_k)
            combs_k+=1
        combs_k-=1
        m+=self.get_combs(c,combs_k)
        if combs_k==1:
            return (m,)

        return self.get_combs_k(c,combs_k,m)

    def calc_comb2(self,en,a,b):
            row_n=0
            if a!=0:
                row_n=self.sumrows(en-1,en-a)
            return int(row_n+(b-a))-1

    def sumrows(self,en,a):
            if en==a:
                return a
            return int((en+a)*(en-a+1)/2)

    def sum_combs(self,N,k):
        return sum(self.get_combs(N,ik) for ik in range(1,k))


    def get_m_list(self,N,lis,indFromStart=True):
        if lis is None:
            return

        if type(lis)!=np.ndarray: lis=np.array(lis)
        combs_k=len(lis)

        m=0
        if  indFromStart:
            m=self.sum_combs(N,combs_k)


        if combs_k==1:
            return lis[0]
        if combs_k==2:
            return m+self.calc_comb2(N,*lis)
        first_ind=lis[0]

        m+=sum([self.get_combs(N-inrow,combs_k-1) for inrow in range(1,first_ind+1)])

        nN=N-first_ind-1
        m+=self.get_m_list(nN,lis[1:]-(first_ind+1),indFromStart=False)
        return m


# # In[111]:

# graphF=GraphFunc(doDirected=False)
# graphF.load_file('london_transport_multiplex.csv',delimiter=',',skiprows=0)
# graphF.make_direct_neighbours(subOne=True)
# graphF.make_zero_orbit()
# graphF.count_tri_graphs()
# print(graphF.comb2_num())
# # graphF.graph_t,\
# graphF.graphNeigh,'\n\n',graphF.save_orbits('london_multi2.csv')
# # graphF.triGNMulti


# # In[68]:

# import networkx as nx
# import os,sys


# graphF=GraphFunc(doDirected=False)

# nodes=7
# degree=2

# els=[]
# for p in [0.5,0.8]:
#     els.append(nx.powerlaw_cluster_graph(nodes,degree,p,seed=12).edges())
# # print(els)

# graphF.load_edge_list(els)


# graphF.make_direct_neighbours()
# graphF.count_tri_graphs()
# # sol=graphF.return_orbits_Mat().values
# graphF.graphNeigh,graphF.graph_t,

# # graphF.triGNMulti
# graphF.save_orbits('dva_plexa_pl.csv')


# # In[4]:

# import networkx as nx
# graphF=GraphFunc(doDirected=False)

# nodes=100
# degree=7

# els=[]
# for p in [12,13]:
#     els.append(nx.barabasi_albert_graph(nodes,degree,seed=p).edges())
# # print(len(els))

# graphF.load_edge_list(els)


# graphF.make_direct_neighbours()
# print(graphF.graphNeigh[0])

# graphF.count_tri_graphs()
# sol=graphF.return_orbits_Mat().values
# # graphF.save_orbits('dva_plexa_ba.csv')


# # In[2]:

# graphF=GraphFunc(doDirected=True)
# graphF.load_file('adj_list_brain.csv',delimiter=',',skiprows=0)
# graphF.make_direct_neighbours()
# graphF.count_tri_graphs()
# print(graphF.comb2_num())

# print('iters',[(ind,i) for ind,i in enumerate(itertools.product('+-o','+-0' ))])


# graphF.save_orbits('saved_orbs2.csv')

# sol=graphF.return_orbits_Mat().values

# tamsol=np.loadtxt('tamara_comparison/signaturevector2.csv',delimiter=',').astype(int)

# print('vertDikt',graphF.edgeN)


# print('graph_neigh','++++++++++\n\n  first orbital :{}/0'.format(
#      np.sum(sol[:,:9]!=tamsol[:, [4,3,5,7,6,8,10,9,11]] )  ))

# print('graph_neigh','++++++++++\n\n  third orbital :{}/0'.format(
#      np.sum(np.sum(sol[:,:9],axis=1)\
#         != np.sum(tamsol[:, [4,3,5,7,6,8,10,9,11]] ,axis=1))  ))


# print(sol[0,:9],sum(sol[0,:9]),'graph graphneigh',graphF.triGNMulti[0][0])
 
# # np.savetxt('big_adjlist_adj.csv',graphF.graph_t,delimiter=',',fmt='%3d')

# # solutions=np.zeros((0,18+9+6),dtype=int)
# # sols=[]
# # for i in range(3):
# #     sols.extend(sol[i][0].values() )

# # solutions=np.vstack((solutions,sols))
# # set(solutions[0])-set(tamsol[0])
# # sum(solutions[0,:9]),sum(tamsol[0,3:3+9])
# # (solutions[0,:9],),(tamsol[0,3:3+9],)


# # In[207]:

# a={1: 2, 2: 2, 3: 2, 4: 1, 5: 1, 6: 2, 7: 2, 8: 1, 9: 2, 10: 2, 11: 1, 12: 2, 13: 1, 14: 1, 15: 1, 16: 1, 17: 1, 18: 1, 19: 1, 20: 2, 21: 1, 22: 1, 23: 1, 24: 1, 25: 2, 26: 1, 27: 1, 28: 1, 29: 1, 30: 2, 31: 1, 32: 1, 33: 1, 34: 1, 35: 1, 36: 1, 37: 1, 38: 2, 39: 2, 40: 1, 41: 1, 42: 1, 43: 2, 44: 1, 45: 1, 47: 1, 48: 1, 53: 1, 69: 1, 70: 1, 73: 1, 85: 1, 88: 1, 92: 1, 97: 1, 99: 1, 105: 1, 106: 1, 107: 1}
# a[2],a[9]


# # In[1]:

# import itertools as iter

# tmpe='0 1 0 2 0 3 0 4 1 2 3 4'.split()

# tmpe2='0 1 0 2 0 3 0 4 1 3 2 4'.split()
# edgelist=[(int(i),int(j)) for i,j in zip(tmpe[::2],tmpe[1::2]) ]
# edgelist2=set([(int(i),int(j)) for i,j in zip(tmpe2[::2],tmpe2[1::2]) ])


# edgelist

# for ind,perms in enumerate(iter.permutations(range(5))):
#     edget=set([(perms[i],perms[j]) for i,j in edgelist])
#     if ind<15:
#         print (edget)
    
#     if edget==edgelist2:
#         print (ind)


# # In[101]:

# type('gfj')


# # In[3]:

# a={1:3,2:4,6:3}
# b={1:3,2:4,17:3,45:3,42:42}
# b.keys()-a.keys()


# # In[28]:

# first='tamara_comparison/first_subject.csv'

# mat=np.loadtxt(first,delimiter=',')
# mat=(mat!=0).astype(int)
# def get_list(mat):
#     mat1=np.triu(mat)
#     mat2=np.tril(mat)
    
#     print(mat[:12,:4])
    
#     adj_list=np.zeros((0,3),dtype=int)

#     def add_row(a,*args):
#         return np.vstack((a,args))

#     for ind,row in enumerate(mat):
#         for ind2,x in enumerate(row):
#             if x==1:
#                 edget=0
#                 if mat[ind2,ind]==1 and ind>ind2: edget=2
#                 elif mat[ind2,ind]==1: continue
#                 if (ind==0 and ind2==9) or (ind==9 and ind2==0):
#                     print('edget',ind,ind2,edget)
#                 adj_list=add_row(adj_list,ind,ind2,edget)

# #     for ind,row in enumerate(mat2):
# #         for ind2,x in enumerate(row):
# #             if x==1:
# #                 edget=1
# #                 if mat[ind2,ind]==1: continue
# #                 adj_list=add_row(adj_list,ind,ind2,edget)
#     return adj_list




# # mat=(np.random.rand(8,8)>0.5).astype(int)
# # for i in range(mat.shape[0]):
# #     mat[i,i]=0
    
    
# adj_list=get_list(mat)


# print(mat[:12,:4])
# np.savetxt('tamara_brain.csv',mat,fmt='%3d',delimiter=',')

# np.savetxt('adj_list_brain.csv',adj_list,fmt='%3d',delimiter=',')
# adj_list


# # In[94]:

# graph=np.loadtxt('test_struct_multi2.csv',delimiter=',').astype(int)


# graph=np.loadtxt('big_adjlist_adj.csv',delimiter=',').astype(int)



# graph=list(graph)

# graph.sort(key=lambda x: -x[2])



# mat=np.zeros((459,459),dtype=int)

# for ind,(i,j,k) in enumerate(graph):
#     if i==j:
#         print(ind)
#     if k==2:
#         mat[i,j]=1
#         mat[j,i]=1
#     elif k==1:
#         mat[j,i]=1
#     else:
#         mat[i,j]=1
        
# print(sum(mat.diagonal()))
# np.savetxt('tamara_big.csv',mat,delimiter=',',fmt='%3d')


# # In[ ]:



